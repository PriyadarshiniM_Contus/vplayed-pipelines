#!groovy

/**
 * Build docker image, with the health check
 *
 * @param config
 * @return
 */
def call(Map config) {
	// Get the docker name
	String imageName = "${config.imageName}".toLowerCase()
	String version = ""
	String contextName = "";
	String imageTag = "${BUILD_NUMBER}"

	if(config.contextName != null ) {
		contextName = config.contextName;
	}else {
		contextName = ".";
	}

	sh "echo imageTag ${BUILD_NUMBER}"

	def buildProps = readProperties file: 'build.properties'

	sh "docker build -t ${imageName}:${imageTag}  --no-cache=true ${contextName}"
}
