#!groovy

/**
 * Run setup shell script for packing software
 *
 * @param config
 * @return
 */
def call(Map config) {

    // Create setup file in the parent directory
    def setupContent = libraryResource "${config.module}/shell/setup"
    writeFile encoding: 'UTF-8', file: 'setup.sh', text: "${setupContent}"

    // Change mode and Run setup
    sh "chmod +x setup.sh && ./setup.sh"
}



