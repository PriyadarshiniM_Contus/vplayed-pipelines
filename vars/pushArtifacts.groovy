#!groovy

import com.contus.cd.Constant

/**
 * Push Artifacts to nexus repository
 *
 * @param config - classifier, file, ext
 * @return
 */
def call(Map config) {
    String version = ""
    String repository = ""

    if ("${BRANCH_NAME}".toLowerCase().contains("release/")) {
        version = "${BRANCH_NAME}".replace("*/", "").replace("release/", "")
        // Not a release re-tag push
        if(!config.release){
            version = "${version}_${BUILD_NUMBER}"
        }
        repository = "maven-releases/"
    } else {
        version = "${BUILD_NUMBER}-SNAPSHOT"
        repository = "maven-snapshots/"
    }

    nexusArtifactUploader artifacts: [[artifactId: "${Constant.APP_NAME.toLowerCase()}_${config.ext}", classifier:
            "${config.classifier}", file: "${config.file}", type : "${config.ext}"]], credentialsId: 'Jenkins-Nexus',
            groupId: 'com.versanetworks', nexusUrl: 'nexus-versa.contus.us:8081', nexusVersion: 'nexus3', protocol:
            'http', repository:"${repository}", version: "${version}"
}



