#!groovy

/**
 * Check if the config already exist
 * Create config if not exist
 *
 * @return boolean is Config Exist
 */
def call(Map config) {

    String SERVICE_NAME = "${config.serviceName}_${config.environment}"

    // Get existing configuration
    String existingConfig = getExistingConfig("${config.module}", "${config.environment}")
    println("oldDockerConfig: ${existingConfig}")

    /**
     * Create New Configuration
     */
    String newDockerConfig = "${config.module}-${config.environment}-env-${BUILD_NUMBER}"

    // Load or read the resource env content
    def envFileContent = libraryResource "${config.module}/${config.environment}"

    // Create a file env in the workspace
    writeFile encoding: 'UTF-8', file: 'config', text: "${envFileContent}"
    sh "docker config create ${newDockerConfig} config"

    // Load or read the resource pipeline properties content
    String pipelinePropText = libraryResource "${config.module}/pipeline.properties"
    def pipelineProps = readProperties text: "${pipelinePropText}"

    // Prepare config source and target
    def addDockerConfig = "--config-add source=${newDockerConfig},target=${pipelineProps['pipeline.config.env.target']}"

    String dockerConfig = addDockerConfig
    if (existingConfig.length() != 0) {
        dockerConfig = "--config-rm ${existingConfig} ${addDockerConfig}"
    }
    try {
        // Service update with new config and remove old config
        sh("docker service update ${dockerConfig} ${SERVICE_NAME} --detach=false")
        if (existingConfig.length() != 0) {
            sh("docker config rm ${existingConfig}")
        }
    }
    catch (java.lang.Exception e) {
        currentBuild.result = "FAILURE"
        error "Build failed: ${e.toString()}"
    }

    // Build Status
    currentBuild.result = "SUCCESS"
}


String getExistingConfig(String module, String env){
    String configName = sh returnStdout: true, script: "docker config ls --filter=name=${module}-${env}-env " +
            "--format \"{{.Name}}\""

    if (configName.length() != 0) {
        return configName.trim()
    }else{
        return ""
    }
}
