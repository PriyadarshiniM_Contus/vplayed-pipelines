#!groovy

/**
 * This method is used to send the QA process request email.
 *
 * @params body (to emails)
 * @return None
 */
def call(Map config) {
    def version = ""
    if ("${BRANCH_NAME}".toLowerCase().contains("release/")) {
        version = "Release "
        version += "${BRANCH_NAME}".replace("*/", "").replace("release/", "")
    } else {
        version = "Build #${BUILD_NUMBER}"
    }

    String attachment = ""
    if (config.attachment != null && "${config.attachment}".length() > 0) {
        attachment = ", ${config.attachment}"
    }

    emailext attachLog: false, attachmentsPattern: "**/report.html, **/releasenote.txt${attachment}", body:
            getQARequestEmailContent(version), subject: 'QA Process - $PROJECT_NAME - Build #$BUILD_NUMBER',
            to: "QAgroup@contus.in, ${config.to}"
}

/**
 * Retrieve's and prepare QA Request email content.
 */
String getQARequestEmailContent(String version) {
    def checkFileExist = fileExists 'qamail.properties'
    if (checkFileExist) {
        echo("true");
        prepareTableContent().replace("%MAIN_CONTENT%", "The Release ${version} is ready for QA process. Kindly check the " +
                "details of the build, given below and start the QA process.")
                .replace("%SUB_CONTENT%", "Provide the Status of the QA Process, in pipeline URL given below" +
                ".<br/><br/>Pipeline URL: ${JOB_URL}")
                .replace("%TEAM%", "Build Team.");
    } else {
        currentBuild.result = "FAILED"
        currentBuild.description = "qamail.properties file not found"
        throw new hudson.AbortException("qamail.properties file not found")
    }
}

/**
 *
 * Prepare table content for QA Request email
 *
 * @param url - Frontend URL
 * @return
 */
String prepareTableContent() {
    def qamailProperties = readFile encoding: 'UTF-8', file: "qamail.properties"

    Properties propSonar = new Properties();
    // load a properties string
    propSonar.load(new StringReader(qamailProperties));

    def projectId = propSonar.getProperty("PROJECT_ID");
    def projectName = propSonar.getProperty("PROJECT_NAME");
    def platform = propSonar.getProperty("PLATFORM");

    def sprint = propSonar.getProperty("SPRINT");
    def component = propSonar.getProperty("COMPONENT");
    def phaseNo = propSonar.getProperty("PHASE_NO");
    def qaTaskId = propSonar.getProperty("QA_TASK_ID");
    def developerName = propSonar.getProperty("DEVELOPER_NAME");

    def backendURL = propSonar.getProperty("BACKEND_URL");
    def backendCredentials = propSonar.getProperty("BACKEND_CREDENTIALS");
    def frontendURL = propSonar.getProperty("FRONTEND_URL");

    def pcrLink = propSonar.getProperty("PCR_LINK");
    def note = propSonar.getProperty("NOTE");

    def emailContent = libraryResource "templates/qaemail_template.html"

    emailContent.replace("%PROJECT_ID%", projectId).replace("%PROJECT_NAME%", projectName)
            .replace("%PLATFORM%", platform).replace("%SPRINT%", sprint).replace("%COMPONENT%", component)
            .replace("%BUILD_NO%", BUILD_NUMBER).replace("%PHASE_NO%", phaseNo)
            .replace("%TASK_ID%", qaTaskId).replace("%DEVELOPER_NAME%", developerName)
            .replace("%BACKEND_URL%", backendURL).replace("%BACKEND_CREDNTIALS%", backendCredentials)
            .replace("%FRONTEND_URL%", frontendURL).replace("%PCR_URL%", pcrLink).replace("%NOTE%", note);
}
