#!groovy

/**
 * Run selenium test base on the platform
 * Read the pipeline properties (Repo url and branch) of the platform
 *
 * @param config (platform)
 * @return none
 */
def call(Map config){

    def sureFire = ""

    def ignore = true
    if(config.containsKey("failBuild")) {
        ignore = !config.failBuild
    }

    if(config.containsKey("environment")){
        String env = "${config.environment}".toLowerCase()
        sureFire = " -Dsurefire.suiteXmlFiles=${env}.xml"
    }

    // Load or read the Dashboard resource pipeline properties content
    String pipelinePropText = libraryResource "dashboard/pipeline.properties"
    def pipelineProps = readProperties text: "${pipelinePropText}"

    sh 'mkdir -p selenium-dashboard'
    dir("selenium-dashboard") {
        git url:"${pipelineProps['pipeline.test.repo_url']}", branch: "${pipelineProps['pipeline.test.repo_branch']}",
                credentialsId: "sathish_bitbucket"

        sh "mvn -Dmaven.test.failure.ignore=${ignore}${sureFire} clean install"
    }

    // Load or read the Shop resource pipeline properties content
    String pipelinePropText1 = libraryResource "shop/pipeline.properties"
    def pipelineProps1 = readProperties text: "${pipelinePropText1}"

    sh 'mkdir -p selenium-shop'
    dir("selenium-shop") {
        git url:"${pipelineProps1['pipeline.test.repo_url']}", branch: "${pipelineProps1['pipeline.test.repo_branch']}",
                credentialsId: "sathish_bitbucket"

        sh "mvn -Dmaven.test.failure.ignore=${ignore}${sureFire} clean install"
    }

}

