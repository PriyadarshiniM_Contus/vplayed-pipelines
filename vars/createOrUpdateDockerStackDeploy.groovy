#!groovy

/**
 * Create or update the docker service.
 * Create volume and mount to the docker service. (only for magento)
 * Remove old docker image.
 *
 *
 * @return
 */
def call(Map config) {
	/**
	 * Docker Service Implementation and Update
	 * Roll out New Version
	 */

	// Get the docker name
	String imageName = "${config.imageName}".toLowerCase()
	String version = ""
	String imageTag = ""
	String dockerConfig = ""
	String moduleName="${config.module}"

	if (config.isDeploy != null && config.isDeploy) { // For deployment pipeline
		imageTag = config.imageTag
	} else {
		if ("${BRANCH_NAME}".toLowerCase().contains("release/")) {
			version = "${BRANCH_NAME}".replace("*/", "").replace("release/", "")
			imageTag = "${version}_${BUILD_NUMBER}"
		} else {
			imageTag = "${BUILD_NUMBER}"
		}
	}

	//Image name
	imageName = "${imageName}:${imageTag}"

	// Load or read the resource pipeline properties content
	String pipelinePropText = libraryResource "api/pipeline.properties"
	def pipelineProps = readProperties text: "${pipelinePropText}"
	def port;
	def environment = "${config.environment}".toLowerCase()
	if(environment.toLowerCase().contains("dev")){
		port = "${pipelineProps['pipeline.deploy.port.dev']}"
	}else if(environment.toLowerCase().contains("qa")){
		port = "${pipelineProps['pipeline.deploy.port.qa']}"
	}else if(environment.toLowerCase().contains("uat")){
		port = "${pipelineProps['pipeline.deploy.port.uat']}"
	}

	def apidockerYaml = libraryResource "all/api.yml"
	apidockerYaml = apidockerYaml.replace("%IMAGE%", "${imageName}").replace("%ENV%",
			"${config.environment}").replace("%MODULE_NAME%", "${config.moduleName}")

	if("${port}"!=null) {		
	apidockerYaml = apidockerYaml.replace("%PORT%", "${port}")
	}
			
	// Create modified yaml file
	writeFile encoding: 'UTF-8', file: 'api.yml', text: "${apidockerYaml}"

	//Stack deploy
	sh "docker stack deploy --compose-file api.yml TCL --with-registry-auth"


	currentBuild.result = "SUCCESS"

}

