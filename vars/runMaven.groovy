#!groovy

/**
 * Run selenium test base on the platform
 * Read the pipeline properties (Repo url and branch) of the platform
 *
 * @param config (platform)
 * @return none
 */
def call(Map config){

    def module = "${config.module}".toLowerCase()

    // Load or read the resource pipeline properties content
    String pipelinePropText = libraryResource "${module}/pipeline.properties"

    def pipelineProps = readProperties text: "${pipelinePropText}"

    sh "mvn clean install"

}

