#!groovy

/**
 * This method is used to send the build failed email.
 *
 * @params body (to emails)
 * @return None
 */
def call(Map config) {

    def emailContent = getEmailContent main: "Build FAILED - ${JOB_NAME} #${BUILD_NUMBER}. Kindly take a look.", sub:
            "Pipeline URL: ${JOB_URL}"

    emailext attachLog: false, body: "${emailContent}", subject: 'Build - $PROJECT_NAME - ' +
            'Build #$BUILD_NUMBER - - $BUILD_STATUS', to: "${config.to}"
}