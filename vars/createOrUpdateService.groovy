#!groovy

/**
 * Create or update the docker service.
 * Create volume and mount to the docker service. (only for JAVA)
 * Remove old docker image.
 *
 *
 * @return
 */
def call(Map config) {
	/**
	 * Docker Service Implementation and Update
	 * Roll out New Version
	 */

	// Get the docker name
	String imageName = "${config.imageName}".toLowerCase()
	String version = ""
	String imageTag = "${BUILD_NUMBER}"
	String moduleName = "${config.moduleName}".toLowerCase()
	String moduleType = "${config.moduleType}"
	String environment = "${config.environment}"
	String serviceName = "${config.serviceName}_${environment}"
	String volumeName = "${config.serviceName}_${environment}"
	String networkName="${config.serviceName}_${environment}"


	// Load or read the resource pipeline properties content
	String pipelinePropText = libraryResource "${moduleType}/pipeline.properties"
	def pipelineProps = readProperties text: "${pipelinePropText}"




	/**
	 * Create and Mount Volume
	 */
	String mountVolume = ""
	def replicas = "1"
	sh "echo Checking Volume :: ${volumeName}"

	String volumeExist = sh returnStdout: true, script: "docker volume ls --filter name=${volumeName} " +
	"--format \"{{.Name}}\""

	if (volumeExist==null||volumeExist.length() == 0) {
		sh "echo Creating Volume ${volumeName}"
		sh "docker volume create ${volumeName}"
	}else {
		sh "echo Volume Already Exist ${volumeName}"
	}


	/**
	 * Create Proxy Network if not exist
	 */
	sh "echo Checking Network :: ${networkName}"


	String networkExist = sh returnStdout: true, script: "docker network ls --filter name=${networkName} --format \"{{.Name}}\""
	if (networkExist==null||networkExist.length() == 0) {
		sh "docker network create --driver overlay ${networkName}"
	}else {
		sh "echo Network Already Exist ${networkName}"
	}

	String serviceExist = sh returnStdout: true, script: "docker service ls --filter name=${serviceName}_${moduleName} --format " +
	"\"{{.Name}}\""


	if (serviceExist==null||serviceExist.length() == 0) {
		sh "echo creating service ${serviceName}_${moduleName}"
		def apidockerYaml;
		if("${config.isPublish}"=='true') {
			apidockerYaml = libraryResource "all/api.yml"
			apidockerYaml = apidockerYaml.replace("%PORT%", "${config.port}")
			sh "echo exposing port"
		}else if("${config.moduleName}"=='configservice'){
			apidockerYaml = libraryResource "all/config_api.yml"
			sh "echo exposing config"
		}else{
			sh "echo not exposing port"
			apidockerYaml = libraryResource "all/local_api.yml"
		}
		apidockerYaml = apidockerYaml.replace("%VOLUME_NAME%", "${volumeName}").replace("%NETWORK_NAME%", "${networkName}").
				replace("%IMAGE%", "${imageName}").replace("%IMAGE_TAG%", "${imageTag}").replace("%ENV%",
				"${environment}").replace("%MODULE_NAME%", "${serviceName}_${moduleName}")
		// Create modified yaml file
		writeFile encoding: 'UTF-8', file: 'api.yml', text: "${apidockerYaml}"
		//Stack deploy
		sh "docker stack deploy --compose-file api.yml TCL --with-registry-auth"
	}else {
		sh "docker service Already Exist ${serviceName}_${moduleName}"

		sh "docker service update --image ${imageName}:${imageTag} ${serviceName}_${moduleName}"

	}


	currentBuild.result = "SUCCESS"

}
