#!groovy

/**
 * This method is used to send the email. (Default)
 *
 * @params body (to emails)
 * @return None
 */
def call(Map config) {

    String attachment = ""
    if (config.attachment != null && "${config.attachment}".length() > 0) {
        attachment = "${config.attachment}"
    }

    emailext attachLog: false, attachmentsPattern: "${attachment}", body: '$DEFAULT_CONTENT', subject: '$PROJECT_NAME - Build ' +
            '#$BUILD_NUMBER - $BUILD_STATUS', to: "${config.to}"
}
