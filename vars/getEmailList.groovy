#!groovy

/**
 * Get the email list from the pipeline properties file.
 *
 * @param config
 * @return
 */
def call(Map config) {
    // Get the platform name
    String module = "${config.module}".toLowerCase()

    // Load or read the resource pipeline properties content
    String pipelinePropText = libraryResource "${module}/pipeline.properties"
    def pipelineProps = readProperties text: "${pipelinePropText}"

    def opsEmail = pipelineProps['pipeline.email.ops']
    def devEmail = pipelineProps['pipeline.email.dev']

    return "${opsEmail}, ${devEmail}"

}



