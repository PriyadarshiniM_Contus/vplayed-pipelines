#!groovy
import com.contus.cd.Constant

/**
 * Build APK
 * Bind Build information like Build number and Build date
 *
 * @param config
 * @return
 */
def call(Map config) {
    String environment = "${config.environment}".toLowerCase().tokenize().collect { it.capitalize() }.join('')
    currentBuild.displayName = "#${BUILD_NUMBER} Build ${environment} APK"

    String version = ""
    String tag = ""

    if("${BRANCH_NAME}".toLowerCase().contains("release/")){
        version = "${BRANCH_NAME}".replace("*/", "").replace("release/", "")
        tag = "${version}_${BUILD_NUMBER}"
    }else{
        tag = "${config.environment}_${BUILD_NUMBER}"
    }

    // Bind Build Number ans Date
    bindBuildInfo module: "Android"

    // Copy and move the release information
    //sh "cp -r releasenote.txt app/src/main/assets/releasenotes/"
    //sh "mv versionnote.txt app/src/main/assets/releasenotes/"

    // Build APK
    if("${config.environment}".contains("dev")){
        environment = "Debug"
    }
    sh "./gradlew clean assemble${environment}"


    // Check for gradle build tool 3+
    // Create a File object representing the folder 'A/B'
    def isFolderExist = fileExists "app/build/outputs/apk/${environment.toLowerCase()}"

    def attachment = ""

    // If it does exist
    if(isFolderExist) { // Gradle build tool 3+
        sh "mv app/build/outputs/apk/${environment.toLowerCase()}/*.apk " +
                "app/build/outputs/apk/${environment.toLowerCase()}/${Constant.APP_NAME.replaceAll("\\s", "")}_${tag}.apk"

        attachment = "**/${environment.toLowerCase()}/*_${tag}.apk"
        archiveArtifacts "${attachment}"


    }else{
        sh "mv app/build/outputs/apk/*.apk app/build/outputs/apk/${Constant.APP_NAME.replaceAll("\\s", "")}_${tag}.apk"

        attachment = "**/*_${tag}.apk"
        archiveArtifacts "${attachment}"
    }


//    String apkFile = "app/build/outputs/apk/${config.environment}/${Constant.APP_NAME.replaceAll("\\s", "")}_${tag}" +
//            ".apk"
//    sh "mv app/build/outputs/apk/${config.environment}/*.apk ${apkFile}"
//    archiveArtifacts "**/*_${tag}.apk"

    //pushArtifacts classifier: "android", file: "${apkFile}", ext: "apk", release: false
}



