#!groovy

/**
 * Push docker image to the nexus repository
 *
 * @param config
 * @return
 */
def call(Map config) {
	// Get the docker name
	String imageName = "${config.imageName}".toLowerCase()
	String version = ""
	String imageTag = "${BUILD_NUMBER}"

	sh "docker push ${imageName}:${imageTag}"
}



