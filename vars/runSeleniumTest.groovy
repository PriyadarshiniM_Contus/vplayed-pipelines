#!groovy

/**
 * Run selenium test base on the platform
 * Read the pipeline properties (Repo url and branch) of the platform
 *
 * @param config (platform)
 * @return none
 */
def call(Map config){

    def module = "${config.module}".toLowerCase()
    def sureFire = ""

    def ignore = true
    if(config.containsKey("failBuild")) {
        ignore = !config.failBuild
    }

    if(config.containsKey("environment")){
        String env = "${config.environment}".toLowerCase()
        sureFire = " -Dsurefire.suiteXmlFiles=${env}.xml"
    }

    // Load or read the resource pipeline properties content
    String pipelinePropText = libraryResource "${module}/pipeline.properties"

    def pipelineProps = readProperties text: "${pipelinePropText}"

    sh 'mkdir -p selenium'
    dir("selenium") {
        git url:"${pipelineProps['pipeline.test.repo_url']}", branch: "${pipelineProps['pipeline.test.repo_branch']}",
                credentialsId: "sathish_bitbucket"

        sh "mvn -Dmaven.test.failure.ignore=${ignore}${sureFire} clean install"
    }

}

