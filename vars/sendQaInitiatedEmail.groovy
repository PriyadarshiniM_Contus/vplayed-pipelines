#!groovy

/**
 * This method is used to send the QA process initiation email.
 *
 * @params body (to emails)
 * @return None
 */
def call(Map config) {

    def emailContent = getEmailContent main: "We have initiated the QA process and will update the status within " +
            "${config.value} Business ${config.metric}.", sub: ""

    emailext attachLog: false, body: "${emailContent}",
            subject: 'QA Process - $PROJECT_NAME - Build #$BUILD_NUMBER', to: "QAgroup@contus.in, ${config.to}"
}