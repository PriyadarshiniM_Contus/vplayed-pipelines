#!groovy
import com.contus.cd.Constant

/**
 * Tag the approved release build and push to nexus repository.
 *
 * @param config
 * @return
 */
def call(Map config) {

    String tag = ""
    String version = ""

    // Get Image Tag
    if ("${BRANCH_NAME}".toLowerCase().contains("release/")) {
        version = "${BRANCH_NAME}".replace("*/", "").replace("release/", "")
        version = "${version}_${BUILD_NUMBER}"
    }else {
        version = "${BUILD_NUMBER}-SNAPSHOT"
    }

    // Pull from Nexus
    pullArtifacts version: "${version}", type: "${config.ext}", classifier: "${config.classifier}",
            dstFileName:"${Constant.APP_NAME.toLowerCase()}_${version}.${config.ext}"

    // Push to Nexus
    pushArtifacts classifier: "${config.classifier}", file: "artifacts/${Constant.APP_NAME.toLowerCase()}_${version}" +
            ".${config.ext}", ext: "${config.ext}", release: true
}



