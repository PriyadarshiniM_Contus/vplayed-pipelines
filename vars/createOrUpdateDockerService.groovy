#!groovy

/**
 * Create or update the docker service.
 * Create volume and mount to the docker service. (only for magento)
 * Remove old docker image.
 *
 *
 * @return
 */
def call(Map config) {
    /**
     * Docker Service Implementation and Update
     * Roll out New Version
     */

    // Get the docker name
    String imageName = "${config.imageName}".toLowerCase()
    String version = ""
    String imageTag = ""

    if (config.isDeploy != null && config.isDeploy) { // For deployment pipeline
        imageTag = config.imageTag
    } else {
        if ("${BRANCH_NAME}".toLowerCase().contains("release/")) {
            version = "${BRANCH_NAME}".replace("*/", "").replace("release/", "")
            imageTag = "${BUILD_NUMBER}"
        } else {
            imageTag = "${BUILD_NUMBER}"
        }
    }

    def environment = "${config.environment}".toLowerCase()
    String SERVICE_NAME = "${config.serviceName}_${environment}"
    String serviceName = sh returnStdout: true, script: "docker service ls --filter name=${SERVICE_NAME} --format " +
            "\"{{.Name}}\""

    // Load or read the resource pipeline properties content
    String pipelinePropText = libraryResource "${config.module}/pipeline.properties"
    def pipelineProps = readProperties text: "${pipelinePropText}"

    def port;
    if(environment.toLowerCase().contains("dev")){
        port = "${pipelineProps['pipeline.deploy.port.dev']}"
    }else if(environment.toLowerCase().contains("qa")){
        port = "${pipelineProps['pipeline.deploy.port.qa']}"
    }else if(environment.toLowerCase().contains("uat")){
        port = "${pipelineProps['pipeline.deploy.port.uat']}"
    }


    // Prepare config source and target
    def addConfig = "source=${config.module}-${environment}-env,target=${pipelineProps['pipeline.config.env.target']}"

    /**
     * Create and Mount Volume
     */
    String mountVolume = ""
    String mountVolume2 = ""
    def replicas = "2"

    if ("${config.module}".contains("node")) {
        String volumeName = sh returnStdout: true, script: "docker volume ls --filter=name=payload-logs " +
                "--format \"{{.Name}}\""

        if (volumeName.length() == 0) {
            sh "docker volume create payload-logs"
        }

        mountVolume = "--mount type=volume,source=payload-logs,target=/var/www/html/public/payload/logs "
    }

    // Setup replicas using config params, if its deploy pipeline
    if (config.replicas != null && config.isDeploy) {
        replicas = "${config.replicas}"
    }

    String serviceImageName = sh returnStdout: true, script: "docker service ls --filter " +
            "name=${SERVICE_NAME} --format " + "\"{{.Image}}\""

    /**
     * Create Proxy Network if not exist
     */
    String networkName = sh returnStdout: true, script: "docker network ls --filter name=proxy --format \"{{.Name}}\""
    if (!networkName.trim().equals("proxy")) {
        sh "docker network create --driver overlay proxy"
    }

    def dockerConfig = ""

    // If Service Name exist the Update Service
    if (serviceName.trim().equals(SERVICE_NAME)) {

        if (config.isConfigExist) {
            dockerConfig = ""
        } else {
            dockerConfig = "--config-add ${addConfig}"
        }

        if (mountVolume != null && mountVolume.length() > 0) {
            mountVolume = mountVolume.replace("--mount", "--mount-add")
        }

        if (mountVolume2 != null && mountVolume2.length() > 0) {
            mountVolume2 = mountVolume2.replace("--mount", "--mount-add")
        }

        if ("${config.module}".contains("proxy")) {
            replicas = "1"
        }

        sh("docker service update --image ${imageName}:${imageTag} ${SERVICE_NAME}  " +
                "--replicas ${replicas} ${dockerConfig} ${mountVolume} ${mountVolume2} --update-delay 10s")
    } else {
        // Get the old container name. This is for the first time
        String oldContainerName = sh returnStdout: true, script: "docker ps --filter=name=${SERVICE_NAME} " +
                "--format \"{{.Names}}\""

        //If old container name available Remove it
        if (oldContainerName.length() != 0) {
            sh("docker rm -f ${oldContainerName}");
        }


        if (config.isConfigExist) {
            String configName = sh returnStdout: true, script: "docker config ls --filter name=${config.module}-${environment}-env " +
                    "--format \"{{.Name}}\""
            if (configName.length() != 0) {
                dockerConfig = "--config source=${configName.trim()},target=${pipelineProps['pipeline.config.env.target']}"
            }else{
                dockerConfig = ""
            }
        } else {
            dockerConfig = "--config ${addConfig}"
        }

        if ("${config.module}".contains("proxy")) {
            replicas = "1"
            // If Service Name does not exist Create Service
            sh("docker service create --name ${SERVICE_NAME} ${dockerConfig} --replicas ${replicas} " +
                    "${mountVolume}--publish mode=host,target=443,published=443 --network proxy ${imageName}:${imageTag}")
        } else {
            // If Service Name does not exist Create Service
            sh("docker service create --name ${SERVICE_NAME} ${dockerConfig} --replicas ${replicas} " +
                    "${mountVolume} ${mountVolume2}--network proxy --publish ${port} ${imageName}:${imageTag}")
        }
    }

    if (config.isDeploy == null || !config.isDeploy) {
        // Remove the last version docker image
        if (serviceImageName.length() != 0) {
            // Sleep 10 Seconds
            sleep 10;

            sh("docker rmi -f ${serviceImageName}");
        }
    }

    currentBuild.result = "SUCCESS"

}