#!groovy

import com.contus.cd.Constant

/**
 * Read the properties sonar-project.properties.
 * Create new project key and project name.
 * Load Sonarqube scanner tool from jenkins.
 * Run Sonarqube scanner, with the properties.
 *
 * @return none
 */
def call(Map config) {

    if("${config.platform}".toLowerCase().contains("java")){
        sh "mvn clean compile package"
    }

    if("${config.platform}".toLowerCase().contains("android")){
        def checkLocalPropsFileExist = fileExists "local.properties"
        if(checkLocalPropsFileExist){
            sh "rm -rf local.properties"
        }
        sh "./gradlew clean assembleDebug"
    }

    // Prepare Project Key from the branch name
    String key = BRANCH_NAME.replace("*/", "").replace("/", "-");

    // Read and Load properties
    def sonarProp = readFile encoding: 'UTF-8', file: "${config.sonarProperties}"

    Properties propSonar = new Properties();
    propSonar.load(new StringReader(sonarProp));

    // Create new project key and project name.
    def sonarProjectKey = propSonar.getProperty("sonar.projectKey") + "_" + key.toUpperCase();
    def sonarProjectName = propSonar.getProperty("sonar.projectName") + "-" + key;

    // Load SonarQube Scanner Tool from Jenkins
    def sonarqubeScannerHome = tool name: 'SonarQubeScanner', type: 'hudson.plugins.sonar.SonarRunnerInstallation'

    // Run SonarQube Scanner
    sh "${sonarqubeScannerHome}/bin/sonar-scanner -Dproject.settings=sonar-project.properties " +
            "-Dsonar.projectKey=${sonarProjectKey} -Dsonar.projectName='${sonarProjectName}'"

    // Setup the Sonarqube project key in the Constant which can be used in the same pipeline
    Constant.SONAR_PROJECT_KEY = sonarProjectKey
}
