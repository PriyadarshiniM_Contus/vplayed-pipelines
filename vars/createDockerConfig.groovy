#!groovy

/**
 * Check if the config already exist
 * Create config if not exist
 *
 * @return boolean is Config Exist
 */
def call(Map config) {

    // Check if docker config exist
    // Create config is not exist
    if (!isConfigExist("${config.module}", "${config.environment}")) {
        def module = "${config.module}".toLowerCase()

        // Load or read the resource env content
        def envFileContent = libraryResource "${module}/${config.environment}"

        // Create a file env in the workspace
        writeFile encoding: 'UTF-8', file: 'env', text: "${envFileContent}"

        // Create config
        sh "docker config create ${module}-${config.environment}-env env"

        return false
    }

    return true
}

/**
 * 1. Check if docker config exist
 * 2. Create config if not exist
 *
 * @param environment
 */
boolean isConfigExist(String module, String env) {
    module = module.toLowerCase()
    String configName = sh returnStdout: true, script: "docker config ls --filter=name=${module}-${env}-env " +
            "--format \"{{.Name}}\""

    if (configName.length() == 0) {
        return false
    } else {
        return true
    }
}
