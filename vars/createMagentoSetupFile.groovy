#!groovy

/**
 * Create magento setup file
 *
 * @param config
 * @return
 */
def call(Map config) {
//    String dbHost = "192.168.2.28"
//    String userName = "root"
//    String password = "M@rooT@987"
//
//    // Versa
//    if ("${config.environment}".toLowerCase().equals("uat")) {
//        dbHost = "192.168.30.60"
//        userName = "versadb"
//        password = "Ver@Ka98Ij"
//    }
//
//    // Verizon
//    if("${config.environment}".toLowerCase().contains("verizon")){
//        dbHost = "192.168.30.20"
//        userName = "versadb"
//        password = "Ver@Ka98Ij"
//    }

    //Create setup.sh
    String setup = "cd /var/www/html/ && \\\n" +
            "composer -g config http-basic.repo.magento.com 06d74b9b1150f125101afc9e99d1b842 5f4cc8776ac098e2e1395d03cd5b0707 && \\\n" +
            "composer install"

    writeFile encoding: 'UTF-8', file: 'setup.sh', text: "${setup}"
}

