#!groovy
/**
 * Bind Build information like Build number and Build date
 *
 * @param config
 * @return
 */
def call(Map config) {
    // Get the module name
    String module = "${config.module}".toLowerCase()
    String version = ""
    String buildNumber = ""

    if("${BRANCH_NAME}".toLowerCase().contains("release/")){
        version = "${BRANCH_NAME}".replace("*/", "").replace("release/", "")
        buildNumber = "${version} (build ${BUILD_NUMBER})"
    }else{
        buildNumber = "#${BUILD_NUMBER}"
    }

    if(module.contains("ios")){
        writeBuildNumberDateToiOSApp()
    }else {
        writeBuildNumberDateToReleaseNote(buildNumber)
    }

    if (module.contains("dashboard")) {
        //writeBuildNumberDateDashboard(module, buildNumber)
    } else if (module.contains("api")) {
        writeBuildNumberDateAPI(buildNumber)
    } else if (module.contains("node")) {
        writeBuildNumberDateDashboard(module, buildNumber)
    } else if(module.contains("android")){
        writeBuildNumberDateToAndroidApp()
    }
}

/**
 * Bind Build Number and Date in the releasenote.txt file
 */
void writeBuildNumberDateToReleaseNote(String buildNumber){
    def date = getDate()
    sh "sed -i \"s|#BUILD_NO#|Build Number: ${buildNumber}|g\" releasenote.txt"
    sh "sed -i \"s|#DATE#|Date: ${date}|g\" releasenote.txt"
}

/**
 * Bind Build Number and Date in the json file
 */
void writeBuildNumberDateDashboard(String module, String buildNumber) {
    def date = getDate()
    String buildInfo = libraryResource "${module}/build.txt"

    // Replace the build no and date in the file
    buildInfo = buildInfo.replace("#BUILD_NO#", "${buildNumber}").replace("#BUILD_DATE#", "${date}")

    // Create the new JSON file before build is triggered
    writeFile encoding: 'UTF-8', file: 'build.js', text: "${buildInfo}"
}

void writeBuildNumberDateAPI(String buildNumber) {
    def date = getDate()
    sh "sed -i \"s|#BUILD_NO#|${buildNumber}|g\" build.properties"
    sh "sed -i \"s|#BUILD_DATE#|${date}|g\" build.properties"
}

/**
 * Bind Build Number and Date in the phtml file
 */
void writeBuildNumberDateMagentoFooter(String buildNumber) {
    def date = getDate()
    sh "sed -i \"s|#BUILD_NO#|${buildNumber}|g\" copyright.phtml"
    sh "sed -i \"s|#DATE#|${date}|g\" copyright.phtml"
}

/**
 * Bind Build Number and Date in the php file
 */
void writeBuildNumberDateAdminDesignFooter() {
    def date = getDate()
    sh "sed -i \"s|#BUILD_NO#|${BUILD_NUMBER}|g\" bottom-footer.php"
    sh "sed -i \"s|#DATE#|${date}|g\" bottom-footer.php"

    sh "sed -i \"s|#BUILD_NO#|${BUILD_NUMBER}|g\" monitor-bottom-footer.php"
    sh "sed -i \"s|#DATE#|${date}|g\" monitor-bottom-footer.php"
}


/**
 * Bind Build Number and Date in the versioninfo.txt file
 */
void writeBuildNumberDateToAndroidApp(){
    def date = getDate()
    sh "sed -i \"s|#BUILD_NO#|${BUILD_NUMBER}|g\" versionnote.txt"
    sh "sed -i \"s|#DATE#|${date}|g\" versionnote.txt"
}

/**
 * Bind Build Number and Date in the versioninfo.txt file
 */
void writeBuildNumberDateToiOSApp(){
    def date = getDate()
    sh "find . -type f -name \"releasenote.txt\" | xargs sed -i '' 's/#BUILD_NO#/Build Number: #${BUILD_NUMBER}/g'"
    sh "find . -type f -name \"releasenote.txt\" | xargs sed -i '' 's/#DATE#/Date: ${date}/g'"

    sh "find . -type f -name \"versioninfo.txt\" | xargs sed -i '' 's/#BUILD_NO#/Build Number: #${BUILD_NUMBER}/g'"
    sh "find . -type f -name \"versioninfo.txt\" | xargs sed -i '' 's/#DATE#/Date: ${date}/g'"
    
}

/**
 * Get the current date
 * @return def Date
 */
def getDate() {
    def date = new Date()
    return date.format("MMMMM d, Y")
}



