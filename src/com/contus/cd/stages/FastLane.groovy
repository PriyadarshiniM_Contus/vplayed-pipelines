package com.contus.cd.stages

import com.contus.cd.Constant
import com.contus.cd.helpers.JenkinsHelper
import com.contus.cd.helpers.Notification
import com.contus.cd.stages.android.APK
import com.contus.cd.stages.ios.IPA
import com.contus.cd.stages.ios.IOSIPA


public class FastLane extends AbstractStage {

	def SONAR_PROP_FILEPATH = "sonar-project.properties"
	def SONAR_QUALITY_GATE_API = "${Constant.SONAR_HOST_URL}/api/qualitygates/project_status?format=json&projectKey="

	def EMAIL_TEMPLATE_REPO = "https://bitbucket.org/vplayed/jenkins-pipeline-email-templates"
	def EMAIL_TEMPLATE_BRANCH = "feature/phase1"

	FastLane(Object script, JenkinsHelper jenkinsHelper) {
		super(script, 'Store Release', jenkinsHelper)
	}
    /**
     * Retrieve's and prepare Deployment successful email content.
     */
    String getDeploymentSuccessfulEmailContent(def env, def url) {
        convertHTMLToString("templates/email_template.html").replace("%MAIN_CONTENT%", "Deployed Successfully in " +
                "${env} - ${script.env.JOB_NAME} #${script.env.BUILD_NUMBER}").replace("%SUB_CONTENT%", "Firebase URL: ${url}")
                .replace("%TEAM%", "Build Team.");
    }

	@Override
	void execute() {
		script.stage(stageName) {
			script.node("Contus-Xcode9") {

				String build = Constant.PLATFORM.toUpperCase().equals("ANDROID")? "apk":"ipa"

				//Build display name
				script.currentBuild.displayName = "#${script.env.BUILD_NUMBER} Build UAT ${build.toUpperCase()}"

				if(Constant.PLATFORM.toUpperCase().equals("ANDROID")) {
					//Checkout code
					script.checkout script.scm

					// Run fastlane execution
					// Set environment path during runtime
					script.withEnv(["JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk-11.0.4.jdk/Contents/Home/", "ANDROID_HOME=/Users/jenkins/library/Android/sdk/", "PATH=/usr/local/Cellar/maven/3.6.1/bin:/usr/local/bin:/usr/local/sbin:/Users/jenkins/bin:/Library/Java/JavaVirtualMachines/jdk-11.0.4.jdk/Contents/Home/bin:/Users/jenkins/library/Android/sdk/tools:/Users/jenkins/library/Android/sdk/platform-tools:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/bin:/usr/local/bin:/usr/local/sbin:/Users/jenkins/bin:/Library/Java/JavaVirtualMachines/jdk-11.0.4.jdk/Contents/Home/bin:/Users/jenkins/library/Android/sdk/tools:/Users/jenkins/library/Android/sdk/platform-tools:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"]) {
						def testResult = script.sh returnStdout: true, script: "/usr/local/bin/fastlane android beta"
						script.println("testResult: ${testResult}")
					}
				}else if(Constant.PLATFORM.toUpperCase().equals("IOS")){
					// Build IPA Debug Dev
					script.dir("MirrorFly") {
						// FIREBASE
						if (Constant.FIREBASE_DISTRIBUTION == true) {
							// Set environment path during runtime
						script.withEnv(["JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk-11.0.4.jdk/Contents/Home/", "PATH=/Users/jenkins/build-wrapper:/Users/jenkins/sonar-scanner/bin:/usr/local/Cellar/maven/3.6.1/bin:/Library/Java/JavaVirtualMachines/jdk-11.0.4.jdk/Contents/Home/bin:/Users/jenkins/library/Android/sdk/tools:/Users/jenkins/library/Android/sdk/platform-tools:/usr/local/bin:/usr/local/sbin:/usr/local/opt/ruby/bin:/usr/local/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"]) {
								def rubyV = script.sh returnStdout: true, script: "ruby -v"
								script.println("rubyV: ${rubyV}")
								def path = script.sh returnStdout: true, script: "echo $PATH"
								script.println("path: ${path}")
								/*script.sh returnStdout: true, script: "which ruby"
								script.sh returnStdout: true, script: "fastlane -v"
								script.sh returnStdout: true, script: "which fastlane" */
								// Check Gemfile to install dependencies
								def checkGemFileExists = script.fileExists "Gemfile"
								// Installing Dependencies
								if (checkGemFileExists) {
									def bundleInstall = script.sh returnStdout: true, script: "bundle install"
									script.println("bundleInstall: ${bundleInstall}")
									// pod install
									def podInstall = script.sh returnStdout: true, script: "/usr/local/bin/pod install"
									script.println("Pod Install: ${podInstall}")
									def bundleFastlane = script.sh returnStdout: true, script: "bundle exec fastlane beta_firebase --verbose"
									script.println("bundleFastlane: ${bundleFastlane}")
								} else {
									script.currentBuild.result = "FAILURE"
									script.error "Build failed: Gemfile is missing!"
								}
								//Send Success email with Deployment URL attached
                				script.emailext attachLog: false, body: getDeploymentSuccessfulEmailContent("FIREBASE",
                        		Constant.FIREBASE_DISTRIBUTION_URL), subject: '🔵 $PROJECT_NAME - Build # $BUILD_NUMBER - ' + '$BUILD_STATUS', to: "${Notification.getDeveloperEmail()}, ${Notification.getBuildManagerEmail()}"
						}
						} else { // TESTFLIGHT
						// Set environment path during runtime
						script.withEnv(["JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk-11.0.4.jdk/Contents/Home/", "PATH=/Users/jenkins/build-wrapper:/Users/jenkins/sonar-scanner/bin:/usr/local/Cellar/maven/3.6.1/bin:/Library/Java/JavaVirtualMachines/jdk-11.0.4.jdk/Contents/Home/bin:/Users/jenkins/library/Android/sdk/tools:/Users/jenkins/library/Android/sdk/platform-tools:/usr/local/bin:/usr/local/sbin:/usr/local/opt/ruby/bin:/usr/local/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"]) {
							script.println("TESTFLIGHT")
							// pod install
							def podInstall = script.sh returnStdout: true, script: "/usr/local/bin/pod install"
							script.println("Pod Install: ${podInstall}")
						 	// Default Beta Lane
							def testResult = script.sh returnStdout: true, script: "fastlane beta --verbose"
							script.println("testResult: ${testResult}")
							}
						}
					}
				}
				// Build Status
				script.currentBuild.result = "SUCCESS"

				// Sleep 5 Seconds to make sure the artifact are archived
				//script.sleep 5;

			}
		}
	}

}
