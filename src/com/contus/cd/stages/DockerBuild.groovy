package com.contus.cd.stages

import com.contus.cd.Constant
import com.contus.cd.helpers.JenkinsHelper
import com.contus.cd.helpers.Notification
import com.contus.cd.stages.AbstractStage


public class DockerBuild extends AbstractStage {

	DockerBuild(Object script, JenkinsHelper jenkinsHelper) {
		super(script, 'Build Docker Image', jenkinsHelper)
	}

	@Override
	void execute() {
		script.stage(stageName) {
		def PROJECT_REPO_BRANCH = "${script.env.BRANCH_NAME}"
		def environment
		def tag_prefix
		if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.UAT_BRANCH) { 
			 	//Should be "stage"
			    Constant.setNODE("UAT")
				environment = 'uat';
				tag_prefix = 'uat-';
		} else if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.MASTER_BRANCH) { 
				//Should be "master"
			    Constant.setNODE("prodbuild")
				if (Constant.PLATFORM.equals("angular") || Constant.PLATFORM.equals("node")) {
					environment = 'production';
				} else {
					environment = 'prod';
				}
				tag_prefix = 'production-';
		} else if (PROJECT_REPO_BRANCH.toLowerCase().contains(Constant.RELEASE_BRANCH)) { 
				//Should be "release"
			    Constant.setNODE("QA")
				environment = 'qa';
				tag_prefix = 'qa-';
		} 
		else if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.DEVELOP_BRANCH) { 
				//Should be "develop"
				Constant.setNODE("DEV")
				if (Constant.PLATFORM.equals("angular") || Constant.PLATFORM.equals("node")) {
					environment = 'development';
				} else {
					environment = 'dev';
				}
				tag_prefix = 'dev-';
		}
		
			script.node("${Constant.NODE}") {
				// Read the build properties
				//Build display name
				script.println("node :: ${Constant.NODE}")
				script.println("Platform :: ${Constant.PLATFORM}")
				script.currentBuild.displayName = "#${script.env.BUILD_NUMBER} Build "
			
				script.println("environment :: ${environment}")
				String DOCKER_TAG="${script.env.BUILD_NUMBER}"
				// Docker Login
				script.sh "docker login -u ${Constant.DOCKER_USERNAME} -p ${Constant.DOCKER_PASSWORD} ${Constant.DOCKER_REPO}"

				// Docker Build
				script.sh "docker build --build-arg ENVIRONMENT=${environment} -t ${Constant.DOCKER_REPO}/${Constant.DOCKER_IMAGE}:${tag_prefix}v${script.env.BUILD_NUMBER} ."

				// Docker Push
				script.sh "docker push ${Constant.DOCKER_REPO}/${Constant.DOCKER_IMAGE}:${tag_prefix}v${script.env.BUILD_NUMBER}"
				
                def CURRENT_BUILD_NO = script.env.BUILD_NUMBER
				script.println("Docker Current Build version ${CURRENT_BUILD_NO}")
				// Build Status
				script.currentBuild.result = "SUCCESS"
			}	}
	}

}
