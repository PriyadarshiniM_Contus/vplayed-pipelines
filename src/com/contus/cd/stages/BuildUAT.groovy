package com.contus.cd.stages

import com.contus.cd.Constant
import com.contus.cd.helpers.JenkinsHelper
import com.contus.cd.helpers.Notification
import com.contus.cd.stages.android.APK
import com.contus.cd.stages.ios.IPA
import com.contus.cd.stages.ios.IOSIPA


public class BuildUAT extends AbstractStage {

    def SONAR_PROP_FILEPATH = "sonar-project.properties"
    def SONAR_QUALITY_GATE_API = "${Constant.SONAR_HOST_URL}/api/qualitygates/project_status?format=json&projectKey="

    def EMAIL_TEMPLATE_REPO = "https://bitbucket.org/vplayed/jenkins-pipeline-email-templates"
    def EMAIL_TEMPLATE_BRANCH = "feature/phase1"

    BuildUAT(Object script, JenkinsHelper jenkinsHelper) {
        super(script, 'Build App', jenkinsHelper)
    }

    @Override
    void execute() {
        script.stage(stageName) {
            script.node("${Constant.NODE}") {
                String build = Constant.PLATFORM.toUpperCase().equals("ANDROID")? "apk":"ipa"

                //Build display name
                script.currentBuild.displayName = "#${script.env.BUILD_NUMBER} Build UAT ${build.toUpperCase()}"

                if(Constant.PLATFORM.toUpperCase().equals("ANDROID")) {
                    // Build APK Debug Dev
					APK apk = new APK(script, jenkinsHelper)
					apk.build(Constant.UAT_APK_NAME, "Debug","uat")
                }else if(Constant.PLATFORM.toUpperCase().equals("IOS")){
                    // Build IPA Debug Dev
                    IOSIPA ipa = new IOSIPA(script, jenkinsHelper)
				    ipa.build(Constant.UAT_IPA,Constant.UAT_IPA_PROFILE)
                }

                // Build Status
                script.currentBuild.result = "SUCCESS"

                // Sleep 5 Seconds to make sure the artifact are archived
                script.sleep 5;
                script.emailext body: '$DEFAULT_CONTENT', recipientProviders:
                        [[$class: 'CulpritsRecipientProvider'], [$class: 'DevelopersRecipientProvider'],
                         [$class: 'RequesterRecipientProvider']], subject: '🔵 $PROJECT_NAME - Build # $BUILD_NUMBER - ' +
                        '$BUILD_STATUS', to: "${Notification.getDeveloperEmail()}, ${Notification.getBuildManagerEmail()}"
            }
        }
    }

}
