package com.contus.cd.stages

import com.contus.cd.Constant
import com.contus.cd.helpers.JenkinsHelper
import com.contus.cd.helpers.Notification
import com.contus.cd.stages.android.APK
import com.contus.cd.stages.ios.IPA
import com.contus.cd.stages.ios.IOSIPA


public class BuildQA extends AbstractStage {

	def EMAIL_TEMPLATE_REPO = "https://bitbucket.org/vplayed/jenkins-pipeline-email-templates"
	def EMAIL_TEMPLATE_BRANCH = "feature/phase1"

	def approvalComment
	def qaStartInput
	def qaApproveInput

	BuildQA(Object script, JenkinsHelper jenkinsHelper) {
		super(script, 'Build Approval', jenkinsHelper)
	}

	/**
	 * Convert HTML file content to string
	 */
	String convertHTMLToString(def filename) {
		def emailContent = script.readFile encoding: 'UTF-8', file: "${filename}"
		return emailContent;
	}

	/**
	 * Retrieve's and prepare QA Ready email content.
	 */
	String getQAReadyEmailContent(def url) {
		convertHTMLToString("templates/email_template.html").replace("%MAIN_CONTENT%", "The Build is ready for QA " +
				"process. Approve the build and send it for QA.").replace("%SUB_CONTENT%", "<br/>Pipeline URL: " +
				"${script.env.JOB_URL}").replace("%TEAM%", "Build Team.");
	}

	/**
	 * Retrieve's and prepare QA Request email content.
	 */
	String getQARequestEmailContent(def url, def approvalComment) {
		def checkFileExist = script.fileExists 'qamail.properties'
		if (checkFileExist) {
			script.echo("true");
			prepareTableContent(url).replace("%MAIN_CONTENT%", "The Build is ready for QA process. Kindly check the " +
					"details of the build, given below and start the QA process.")
					.replace("%SUB_CONTENT%", "Provide the Status of the QA Process, in pipeline URL given below" +
					".<br/><br/>Pipeline URL: ${script.env.BUILD_URL}")
					.replace("%TEAM%", "Build Team.");
		} else {
			convertHTMLToString("templates/email_template.html").replace("%MAIN_CONTENT%", "The Build is ready for" +
					" QA process. Kindly start the QA process, once you receive mail from Dev Team")
					.replace("%SUB_CONTENT%", "Provide the QA Process Status once started" +
					".<br/><br/>Pipeline URL: ${script.env.JOB_URL}").replace("%TEAM%", "Build Team.");
		}
	}

	void checkQAMailPropertiesFile(){
		def qamailProperties = script.readFile encoding: 'UTF-8', file: "qamail.properties"

		Properties propSonar = new Properties();
		// load a properties string
		propSonar.load(new StringReader(qamailProperties));

		[
			'PROJECT_ID',
			'PROJECT_NAME',
			'PLATFORM',
			'SPRINT',
			'COMPONENT',
			'PHASE_NO',
			'QA_TASK_ID',
			'DEVELOPER_NAME',
			'BACKEND_URL',
			'BACKEND_CREDENTIALS',
			'FRONTEND_URL',
			'PCR_LINK',
			'NOTE'
		].each { property ->
			if(propSonar.getProperty("${property}") == null){
				script.currentBuild.result = "FAILED"
				script.currentBuild.description = "Error qamail.properties"
				throw new hudson.AbortException("Property ${property} is missing/incorrect in qamail.properties file")
			}
		}
	}

	/**
	 *
	 * Prepare table content for QA Request email
	 *
	 * @param url - Frontend URL
	 * @return
	 */
	String prepareTableContent(url) {
		def qamailProperties = script.readFile encoding: 'UTF-8', file: "qamail.properties"

		Properties propSonar = new Properties();
		// load a properties string
		propSonar.load(new StringReader(qamailProperties));

		def projectId = propSonar.getProperty("PROJECT_ID");
		def projectName = propSonar.getProperty("PROJECT_NAME");
		def platform = propSonar.getProperty("PLATFORM");

		def sprint = propSonar.getProperty("SPRINT");
		def component = propSonar.getProperty("COMPONENT");
		def phaseNo = propSonar.getProperty("PHASE_NO");
		def qaTaskId = propSonar.getProperty("QA_TASK_ID");
		def developerName = propSonar.getProperty("DEVELOPER_NAME");

		def backendURL = propSonar.getProperty("BACKEND_URL");
		def backendCredentials = propSonar.getProperty("BACKEND_CREDENTIALS");
		def frontendURL = propSonar.getProperty("FRONTEND_URL");

		def pcrLink = propSonar.getProperty("PCR_LINK");
		def note = propSonar.getProperty("NOTE");

		convertHTMLToString("templates/qaemail_template.html").replace("%PROJECT_ID%", projectId).replace("%PROJECT_NAME%", projectName)
				.replace("%PLATFORM%", platform).replace("%SPRINT%", sprint).replace("%COMPONENT%", component)
				.replace("%BUILD_NO%", script.env.BUILD_NUMBER).replace("%PHASE_NO%", phaseNo)
				.replace("%TASK_ID%", qaTaskId).replace("%DEVELOPER_NAME%", developerName)
				.replace("%BACKEND_URL%", backendURL).replace("%BACKEND_CREDNTIALS%", backendCredentials)
				.replace("%FRONTEND_URL%", frontendURL).replace("%PCR_URL%", pcrLink).replace("%NOTE%", note);
	}

	/**
	 * Retrieve's and prepare General email content.
	 */
	String getEmailContent(def main, def sub) {
		convertHTMLToString("templates/email_template.html").replace("%MAIN_CONTENT%", "${main}")
				.replace("%SUB_CONTENT%", "${sub}").replace("%TEAM%", "Build Team.");
	}

	String approvedBy(String name) {
		return name.toString().replace("_contus", "").toLowerCase().tokenize().collect { it.capitalize() }.join('')
	}

	@Override
	void execute() {
		script.node("${Constant.NODE}") {
			script.stage(stageName) {
				
				if (Constant.PLATFORM.toUpperCase().equals("ANDROID")) {
					//Checkout code
					script.checkout script.scm
				}

				// Check qamail.properties file
				checkQAMailPropertiesFile()

				/**
				 * Checkout the Email Template files
				 */
				script.sh 'mkdir -p templates'
				script.dir("templates") {
					script.git branch: "${EMAIL_TEMPLATE_BRANCH}", credentialsId: "${Constant.CREDENTIAL}",
					url: "${EMAIL_TEMPLATE_REPO}"
				}

				/**
				 * Send email to OPS and Dev team - Build Ready for QA Process.
				 */
				script.emailext attachLog: false, body: getQAReadyEmailContent(""),
				subject: '🔵 Approve the Build for QA process - $PROJECT_NAME - Build # $BUILD_NUMBER - ' +
				'$BUILD_STATUS', to: "${Notification.getBuildManagerEmail()}, ${Notification.getDeveloperEmail()}"

				/**
				 * Ask for approval to proceed for QA Process.
				 */
				script.timeout(time: 1, unit: 'DAYS') {
					//Timeout in 1 day
					approvalComment = script.input id: 'approve_for_qa', message: 'Approve for QA Process', ok:
					'Proceed', parameters: [
						script.choice(name: 'Send QA Mail?', choices: ["Yes", "No"].join
						("\n"), description: 'NO - Build will be updated in QA Server, and mail will not be ' +
						'sent to QA Team')
					], submitter: 'priyadarshini.m@contus.in'
				}
				script.echo("Approval Comment: ${approvalComment}");

				String build = Constant.PLATFORM.toUpperCase().equals("ANDROID") ? "apk" : "ipa"

				// Build display name
				script.currentBuild.displayName = "#${script.env.BUILD_NUMBER} Build QA ${build.toUpperCase()}"

				// Dont build app incase if automation is not running, bcoz it is generated already
				if (Constant.PLATFORM.toUpperCase().equals("ANDROID") && Constant.AUTOMATION == false) {
					APK apk = new APK(script, jenkinsHelper)
					apk.build(Constant.QA_APK_NAME, "Debug","Qa")
				} else if (Constant.PLATFORM.toUpperCase().equals("IOS")) {
					// Build IPA Debug Dev
					IOSIPA ipa = new IOSIPA(script, jenkinsHelper)
					ipa.build(Constant.QA_IPA,Constant.QA_IPA_PROFILE)
				}

				script.currentBuild.result = "SUCCESS"
				sleep 5;
			}

			script.stage("QA Process") {

				if (approvalComment.contains("Yes")) {
					String build = Constant.PLATFORM.toUpperCase().equals("ANDROID") ? "apk" : "ipa"
					/**
					 * 1. Send email to QA Team to start the QA process.
					 */
					script.emailext attachLog: false, attachmentsPattern: "releasenote.txt", body:
					getQARequestEmailContent("http://${script.env.APP_HOST}/${script.env.SERVICE_HOST}",
					approvalComment), subject: 'QA Process - $PROJECT_NAME - Build # $BUILD_NUMBER', recipientProviders:
					[
						[$class: 'CulpritsRecipientProvider'],
						[$class: 'DevelopersRecipientProvider'],
						[$class: 'RequesterRecipientProvider']
					],
					to: "${Notification.getQaEmail()}, ${Notification.getDeveloperEmail()}, ${Notification.getBuildManagerEmail()}"
					try {
						/**
						 * 2. Ask for approval to proceed for QA Process.
						 */
						script.timeout(time: 4, unit: 'DAYS') {
							//Timeout in 4 days
							qaStartInput = script.input id: 'qa_started', message: 'Ready to Start QA Process?', ok:
							'Yes', parameters: [
								script.choice(choices: ['Hours', 'Days'].join("\n"), description: '',
								name: 'Metric'),
								script.string(defaultValue: '1', description: '',
								name: 'Value')
							], submitterParameter: 'approved_by', submitter:
							'priyadarshini.m@contus.in,balaganesh.g@contus.in'
						}
						/**
						 * 3. Send email to OPS and Dev team - QA Process started.
						 */
						String qaInitiatedEmailTxt = "We have initiated the QA process and will update the status " +
								"within ${qaStartInput['Value']} Business ${qaStartInput['Metric']}."

						script.emailext attachLog: false, body: getEmailContent("${qaInitiatedEmailTxt}", "Initiated" +
						" By: ${qaStartInput['approved_by']}"),
						subject: 'QA Process - $PROJECT_NAME - Build # $BUILD_NUMBER', to: "${Notification.getQaEmail()}, " +
						"${Notification.getDeveloperEmail()}, ${Notification.getBuildManagerEmail()}"

						/**
						 * 5. Automated test using selenium or appium.
						 */
						//node("selenium-node") {
						//   bat '%BATCH_HOME%\\versa\\web.bat'
						//}

						/**
						 * 7. Approve/Decline the build, by Authorised QA person.
						 */
						script.timeout(time: 5, unit: 'DAYS') {
							//Timeout in 5 days
							qaApproveInput = script.input id: 'qa_approval', 
												message: 'Approve the Build?', 
												ok: 'I Approve', 
												parameters: [
													script.string(defaultValue: 'QA Report', description: '', name: 'Report')
												],
												submitterParameter: 'approved_by', 
												submitter: "priyadarshini.m@contus.in,balaganesh.g@contus.in}"
						}

						/**
						* 8. Send email to OPS and Dev team about the status.
						*/
						String qaApprovedEmailTxt = "QA Report - ${qaApproveInput['Report']}."
						script.emailext attachLog: false, body: 
						getEmailContent("QA PASSED - ${script.env.JOB_NAME} " +
							"#${script.env.BUILD_NUMBER}. Now the build" + " is ready to deploy in UAT\n\n ${qaApprovedEmailTxt} \n", 
							"Pipeline URL: ${script.env.JOB_URL}\n"), 
						subject: 'QA Process - $PROJECT_NAME -Build # $BUILD_NUMBER', 
						to: "${Notification.getQaEmail()}, ${Notification.getDeveloperEmail()}, ${Notification.getBuildManagerEmail()}"

					}
					catch (err) {
						if (err.toString().toLowerCase().contains("flowinterruptedexception")) {
							script.emailext attachLog: false, body: getEmailContent("QA FAILED - ${script.env.JOB_NAME} " +
							"#${script.env.BUILD_NUMBER}. Kindly fix the bugs and revert for QA Process.", "Pipeline " +
							"URL: ${script.env.JOB_URL}"), subject: 'QA Process - $PROJECT_NAME - Build # ' +
							'$BUILD_NUMBER', to: "${Notification.getQaEmail()}, " +
							"${Notification.getDeveloperEmail()}, ${Notification.getBuildManagerEmail()}"

							script.currentBuild.result = "FAILURE"
							script.error "QA Process Failed"
						}
					}
				}

			}
		}

	}
}
