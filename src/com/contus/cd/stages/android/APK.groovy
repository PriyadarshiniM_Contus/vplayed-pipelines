package com.contus.cd.stages.android

import com.contus.cd.Constant
import com.contus.cd.helpers.JenkinsHelper
import com.contus.cd.helpers.Utils

public class APK implements Serializable {

	def script
	def jenkinsHelper

	APK(Object script, JenkinsHelper jenkinsHelper) {
		this.script = script
		this.jenkinsHelper = jenkinsHelper
	}

	/**
	 * Gradle clean.
	 * Gradle Assemble Debug.
	 * Rename the IPA with build number.
	 * Archive IPA (Jenkins Artifacts).
	 */
	String build(def env, def flavor, def config) {

		// Bind Build Number ans Date
		Utils utils = new Utils(script)
		utils.writeBuildNumberDateToReleaseNote()
		utils.writeBuildNumberDateToAndroidApp()

		//utils.copyFile("releasenote.txt", "app/src/main/assets/releasenotes/")
		//utils.moveFile("versionnote.txt", "app/src/main/assets/releasenotes/")

		String parentFolder = ""
		String gradlePath = "./"
		if (Constant.FOLDERNAME.length() != 0) {
			parentFolder = Constant.FOLDERNAME + "/"
			gradlePath = parentFolder
		}

		def checkFileExist = script.fileExists "${parentFolder}app/contus_internal_keystore.jks"
		if (!checkFileExist) {
			utils.copyFile("/home/devops/contus_internal_keystore.jks", "${parentFolder}app/")
		}

		def checkLocalPropsFileExist = script.fileExists "${parentFolder}local.properties"
		if(checkLocalPropsFileExist){
			script.sh "rm -rf local.properties"
		}
		String environmentTemp="${env}"+"${config}"
		String environment = "${environmentTemp}".toLowerCase().tokenize().collect { it.capitalize() }.join('')
		String buildFolder = "${env}".toLowerCase()+"/"+"${config}".toLowerCase()
		script.println(environment)

		script.sh "chmod +x ${parentFolder}gradlew"
		script.sh "./gradlew clean assemble${environment}"

		// Create a File object representing the folder 'A/B'
		def isFolderExist = script.fileExists "${parentFolder}app/build/outputs/apk/${buildFolder}"

		def attachment = ""

		// If it does exist
		if(isFolderExist) { // Gradle build tool 3+
			script.sh "mv ${parentFolder}app/build/outputs/apk/${buildFolder}/*.apk " +
					// "${parentFolder}app/build/outputs/apk/${buildFolder}/${env}_${script.env.BUILD_NUMBER}.apk"
					"${parentFolder}app/build/outputs/apk/${buildFolder}/${env}.apk"

			// attachment = "**/${buildFolder}/*${script.env.BUILD_NUMBER}.apk"
			attachment = "**/${buildFolder}/${env}.apk"
			script.archiveArtifacts "${attachment}"


		}else{
			// script.sh "mv ${parentFolder}app/build/outputs/apk/${buildFolder}/*.apk ${parentFolder}app/build/outputs/apk/${env}_${script.env.BUILD_NUMBER}.apk"
			script.sh "mv ${parentFolder}app/build/outputs/apk/${buildFolder}/*.apk ${parentFolder}app/build/outputs/apk/${env}.apk"

			attachment = "**/${env}.apk"
			script.archiveArtifacts "${attachment}"
		}

		return attachment.toString()
	}
}
