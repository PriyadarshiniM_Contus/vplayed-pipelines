package com.contus.cd.stages

import com.contus.cd.Constant
import com.contus.cd.helpers.Docker
import com.contus.cd.helpers.JenkinsHelper
import com.contus.cd.helpers.Notification

public class DeployQA extends AbstractStage {

    def EMAIL_TEMPLATE_REPO = "https://bitbucket.org/vplayed/jenkins-pipeline-email-templates"
    def EMAIL_TEMPLATE_BRANCH = "feature/phase1"

    def approvalComment
    def qaStartInput

    DeployQA(Object script, JenkinsHelper jenkinsHelper) {
        super(script, 'Deploy In QA', jenkinsHelper)
    }

    /**
     * Convert HTML file content to string
     */
    String convertHTMLToString(def filename) {
        def emailContent = script.readFile encoding: 'UTF-8', file: "${filename}"
        return emailContent;
    }

    /**
     * Retrieve's and prepare QA Ready email content.
     */
    String getQAReadyEmailContent(def url) {
        convertHTMLToString("templates/email_template.html").replace("%MAIN_CONTENT%", "The Build is ready for QA " +
                "process. Approve the build and send it for QA.").replace("%SUB_CONTENT%", "<br/>Pipeline URL: " +
                "${script.env.JOB_URL}").replace("%TEAM%", "Build Team.");
    }

    /**
     * Retrieve's and prepare QA Request email content.
     */
    String getQARequestEmailContent(def url, def approvalComment) {
        def checkFileExist = script.fileExists 'qamail.properties'
        if (checkFileExist) {
            script.echo("true");
            prepareTableContent(url).replace("%MAIN_CONTENT%", "The Build is ready for QA process. Kindly check the " +
                    "details of the build, given below and start the QA process.")
                    .replace("%SUB_CONTENT%", "Provide the Status of the QA Process, in pipeline URL given below" +
                    ".<br/><br/>Pipeline URL: ${script.env.JOB_URL}")
                    .replace("%TEAM%", "Build Team.");
        } else {
            script.currentBuild.result = "FAILED"
            script.currentBuild.description = "qamail.properties file not found"
            throw new hudson.AbortException("qamail.properties file not found")
        }
    }

    /**
     *
     * Prepare table content for QA Request email
     *
     * @param url - Frontend URL
     * @return
     */
    String prepareTableContent(url) {
        def qamailProperties = script.readFile encoding: 'UTF-8', file: "qamail.properties"

        Properties propSonar = new Properties();
        // load a properties string
        propSonar.load(new StringReader(qamailProperties));

        def projectId = propSonar.getProperty("PROJECT_ID");
        def projectName = propSonar.getProperty("PROJECT_NAME");
        def platform = propSonar.getProperty("PLATFORM");

        def sprint = propSonar.getProperty("SPRINT");
        def component = propSonar.getProperty("COMPONENT");
        def phaseNo = propSonar.getProperty("PHASE_NO");
        def qaTaskId = propSonar.getProperty("QA_TASK_ID");
        def developerName = propSonar.getProperty("DEVELOPER_NAME");

        def backendURL = propSonar.getProperty("BACKEND_URL");
        def backendCredentials = propSonar.getProperty("BACKEND_CREDENTIALS");
        def frontendURL = propSonar.getProperty("FRONTEND_URL");

        def pcrLink = propSonar.getProperty("PCR_LINK");
        def note = propSonar.getProperty("NOTE");

        convertHTMLToString("templates/qaemail_template.html").replace("%PROJECT_ID%", projectId).replace("%PROJECT_NAME%", projectName)
                .replace("%PLATFORM%", platform).replace("%SPRINT%", sprint).replace("%COMPONENT%", component)
                .replace("%BUILD_NO%", script.env.BUILD_NUMBER).replace("%PHASE_NO%", phaseNo)
                .replace("%TASK_ID%", qaTaskId).replace("%DEVELOPER_NAME%", developerName)
                .replace("%BACKEND_URL%", backendURL).replace("%BACKEND_CREDNTIALS%", backendCredentials)
                .replace("%FRONTEND_URL%", frontendURL).replace("%PCR_URL%", pcrLink).replace("%NOTE%", note);
    }

    /**
     * Retrieve's and prepare General email content.
     */
    String getEmailContent(def main, def sub) {
        convertHTMLToString("templates/email_template.html").replace("%MAIN_CONTENT%", "${main}")
                .replace("%SUB_CONTENT%", "${sub}").replace("%TEAM%", "Build Team.");
    }

    /**
     * Retrieve's and prepare Deployment successful email content.
     */
    String getDeploymentSuccessfulEmailContent(def env, def url) {
        convertHTMLToString("templates/email_template.html").replace("%MAIN_CONTENT%", "Deployed Successfully in " +
                "${env} - ${script.env.JOB_NAME} #${script.env.BUILD_NUMBER}").replace("%SUB_CONTENT%", "URL: ${url}")
                .replace("%TEAM%", "Build Team.");
    }

    @Override
    void execute() {
        script.node("${Constant.NODE}") {
            script.stage(stageName) {

                /**
                 * Checkout the Email Template files
                 */
                script.sh 'mkdir -p templates'
                script.dir("templates") {
                    script.git branch: "${EMAIL_TEMPLATE_BRANCH}", credentialsId: "${Constant.CREDENTIAL}",
                            url: "${EMAIL_TEMPLATE_REPO}"
                }

                /**
                 * Send email to OPS and Dev team - Build Ready for QA Process.
                 */
                script.emailext attachLog: false, body: getQAReadyEmailContent("http://versa.qa.contus.us"),
                        subject: '🔵 Approve the Build for QA process - $PROJECT_NAME - Build # $BUILD_NUMBER - ' +
                                '$BUILD_STATUS', to: "${Notification.getBuildManagerEmail()}, ${Notification.getDeveloperEmail()}"

                /**
                 * Ask for approval to proceed for QA Process.
                 */
                script.timeout(time: 1, unit: 'DAYS') { //Timeout in 1 day
                    approvalComment = script.input id: 'approve_for_qa', message: 'Approve for QA Process', ok:
                            'Proceed', parameters: [script.choice(name: 'Send QA Mail?', choices: ["Yes", "No"].join
                            ("\n"), description: 'NO - Build will be updated in QA Server, and mail will not be ' +
                            'sent to QA Team')], submitter: 'priyadarshini.m@contus.in,' +
                            'balaganesh.g@contus.in'
                }
                script.echo("Approval Comment: ${approvalComment}");

                /**
                 * Deploy in QA Environment.
                 */
                // Display name
                script.currentBuild.displayName = "#${script.env.BUILD_NUMBER} DeployInQA"

                // Deploy QA code goes here
                Docker docker = new Docker(script, jenkinsHelper)
                docker.deploy("qa")

                // Build Status
                script.currentBuild.result = "SUCCESS"
                sleep 5;
            }

            script.stage("QA Process") {
                if (approvalComment.contains("Yes")) {
                    /**
                     * 1. Send email to QA Team to start the QA process.
                     */
                    script.emailext attachLog: false, attachmentsPattern: '**/releasenote.txt', body:
                            getQARequestEmailContent("http://${script.env.APP_HOST}/${script.env.SERVICE_HOST}",
                                    approvalComment), subject: 'QA Process - $PROJECT_NAME - Build # $BUILD_NUMBER',
                            to: "${Notification.getQaEmail()}, ${Notification.getDeveloperEmail()}," +
                                    " ${Notification.getBuildManagerEmail()}"
                    try {
                        /**
                         * 2. Ask for approval to proceed for QA Process.
                         */
                        script.timeout(time: 4, unit: 'DAYS') { //Timeout in 4 days
                            qaStartInput = script.input id: 'qa_started', message: 'Ready to Start QA Process?', ok:
                                    'Yes', parameters: [script.choice(choices: ['Hours', 'Days'].join("\n"), description: '',
                                    name: 'Metric'), script.string(defaultValue: '1', description: '',
                                    name: 'Value')], submitter: 'sathishkumar@contus.in,manikandan.guna@contus.in,shivshankar.b@contus.in,sathishkumar.r@contus.in,shanthini.b@contus.in,ramankanth.v@contus.in,nishantpaul.s@contus.in,sangeethrajan.r@contus.in,krishnaraj.b@contus.in,venkatesh.s@contus.in,shamshadgilani.s@contus.in,dinesh.k@contus.in'
                        }
                        /**
                         * 3. Send email to OPS and Dev team - QA Process started.
                         */
                        String qaInitiatedEmailTxt = "We have initiated the QA process and will update the status " +
                                "within ${qaStartInput['Value']} Business ${qaStartInput['Metric']}."

                        script.emailext attachLog: false, body: getEmailContent("${qaInitiatedEmailTxt}", ""),
                                subject: 'QA Process - $PROJECT_NAME - Build # $BUILD_NUMBER', to:"${Notification.getQaEmail()}, " +
                                "${Notification.getDeveloperEmail()}, ${Notification.getBuildManagerEmail()}"

                        /**
                         * Automated test using selenium or appium.
                         */
                        //AutoTest autoTest = new AutoTest(script, jenkinsHelper)
                        //autoTest.execute()

                        /**
                         * Approve/Decline the build, by Authorised QA person.
                         */
                        script.timeout(time: 5, unit: 'DAYS') { //Timeout in 5 days
                            script.input id: 'qa_approval', message: 'Approve the Build?', ok: 'I Approve', submitter:
                                    'sathishkumar@contus.in,manikandan.guna@contus.in,shivshankar.b@contus.in,sathishkumar.r@contus.in,shanthini.b@contus.in,ramankanth.v@contus.in,nishantpaul.s@contus.in,sangeethrajan.r@contus.in,krishnaraj.b@contus.in,venkatesh.s@contus.in,shamshadgilani.s@contus.in,dinesh.k@contus.in'
                        }
                    }
                    catch (err) {
                        if (err.toString().toLowerCase().contains("flowinterruptedexception")) {
                            script.emailext attachLog: false, body: getEmailContent("QA FAILED - ${script.env.JOB_NAME} " +
                                    "#${script.env.BUILD_NUMBER}. Kindly fix the bugs and revert for QA Process.", "Pipeline " +
                                    "URL: ${script.env.JOB_URL}"), subject: 'QA Process - $PROJECT_NAME - Build # ' +
                                    '$BUILD_NUMBER', to: "${Notification.getQaEmail()}, ${Notification.getDeveloperEmail()}, ${Notification.getBuildManagerEmail()}"

                            script.currentBuild.result = "FAILURE"
                            script.error "QA Process Failed"
                        }
                    }
                    /**
                     * 8. Send email to OPS and Dev team about the status.
                     */
                    script.emailext attachLog: false, body: getEmailContent("QA PASSED - ${script.env.JOB_NAME} " +
                            "#${script.env.BUILD_NUMBER}. Now the build" + " is ready to deploy in UAT", "Pipeline " +
                            "URL: ${script.env.JOB_URL}"), subject: 'QA Process - $PROJECT_NAME - Build # ' +
                            '$BUILD_NUMBER', to: "${Notification.getQaEmail()}, ${Notification.getDeveloperEmail()}, " +
                            "${Notification.getBuildManagerEmail()}"

                } else {
                    String url
                    if (Constant.PLATFORM.toUpperCase().equals("DJANGO")) {
                        url = "http://versa.qa.contus.us:3000"
                    } else if (Constant.PLATFORM.toUpperCase().equals("MAGENTO2")) {
                        url = "http://versa.qa.contus.us:4000"
                    } else if (Constant.PLATFORM.toUpperCase().equals("DESIGN")) {
                        url = "http://versa.qa.contus.us:2000"
                    } else if (Constant.PLATFORM.toUpperCase().equals("NODE")) {
                        url = "http://versa.qa.contus.us:8000"
                    }
                    //Send Success email with Deployment URL attached
                    script.emailext attachLog: false, body: getDeploymentSuccessfulEmailContent("QA Environment",
                            "${url}"), subject: '🔵 $PROJECT_NAME - Build # $BUILD_NUMBER - ' +
                            '$BUILD_STATUS', to: "${Notification.getDeveloperEmail()}, ${Notification.getBuildManagerEmail()}"
                }
            }
        }
    }
}