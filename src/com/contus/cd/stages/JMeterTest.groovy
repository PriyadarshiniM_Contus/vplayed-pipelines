package com.contus.cd.stages

import com.contus.cd.Constant
import com.contus.cd.helpers.JenkinsHelper

/**
 * JMeter Test will triggered.
 */
public class JMeterTest extends AbstractStage {

	JMeterTest(Object script, JenkinsHelper jenkinsHelper) {
		super(script, 'JMeter Test', jenkinsHelper)
	}

	@Override
	void execute() {
		script.stage(stageName) {
			script.node("${Constant.NODE}") {
                script.stage ('JMeter Test') {
                    // Get some code from a BitBucket repository
                    script.git branch: "${Constant.API_AUTOMATION_REPO_BRANCH}", credentialsId: "${Constant.CREDENTIAL}",
                    url: "${Constant.API_AUTOMATION_REPO_URL}"
                    def testResult = script.sh returnStdout: true, script: "jmeter -f -n -t /MirrorFly.jmx -l /result.csv"
                    script.println("testResult: ${testResult}")
                }
			}
		}
	}
}
