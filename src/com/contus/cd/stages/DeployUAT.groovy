package com.contus.cd.stages

import com.contus.cd.Constant
import com.contus.cd.helpers.Docker
import com.contus.cd.helpers.JenkinsHelper
import com.contus.cd.helpers.Notification

/**
 * Deploy the Web projects in the development environment.
 * This is also called as "Deploy UAT Pipeline"
 *
 * @author sathishkumar@contus.in
 * @version 1.0
 */
public class DeployUAT extends AbstractStage {

    def EMAIL_TEMPLATE_REPO = "https://bitbucket.org/vplayed/jenkins-pipeline-email-templates"
    def EMAIL_TEMPLATE_BRANCH = "feature/phase1"

    DeployUAT(Object script, JenkinsHelper jenkinsHelper) {
        super(script, 'Deploy In UAT', jenkinsHelper)
    }

    /**
     * Convert HTML file content to string
     */
    String convertHTMLToString(def filename) {
        def emailContent = script.readFile encoding: 'UTF-8', file: "${filename}"
        return emailContent;
    }

    /**
     * Retrieve's and prepare Deployment successful email content.
     */
    String getDeploymentSuccessfulEmailContent(def env, def url) {
        convertHTMLToString("templates/email_template.html").replace("%MAIN_CONTENT%", "Deployed Successfully in " +
                "${env} - ${script.env.JOB_NAME} #${script.env.BUILD_NUMBER}").replace("%SUB_CONTENT%", "URL: ${url}")
                .replace("%TEAM%", "Build Team.");
    }

    @Override
    void execute() {
        script.stage(stageName) {
            script.node("${Constant.NODE}") {

                // Environment based setup
                //script.sh("sed -i \"s|versanetwork.settings|versanetwork.settings.dev|g\" code/versanetwork/wsgi.py")

                // Display name
                script.currentBuild.displayName = "#${script.env.BUILD_NUMBER} DeployInUAT"

                if (Constant.PLATFORM.toUpperCase().equals("MAGENTO2")) {
                    //script.sh "sed -i \"s|developer|production --skip-compilation|g\" Dockerfile"
                }
                // Deploy in Dev
                Docker docker = new Docker(script, jenkinsHelper)
                docker.deploy("uat")

                // Build Status
                script.currentBuild.result = "SUCCESS"

                sleep 5;

                /**
                 * Checkout the Email Template files
                 */
                def checkFileExist = script.fileExists 'templates/email_template.html'
                if (!checkFileExist) {
                    script.sh 'mkdir -p templates'
                    script.dir("templates") {
                        script.git branch: "${EMAIL_TEMPLATE_BRANCH}", credentialsId: "${Constant.CREDENTIAL}",
                                url: "${EMAIL_TEMPLATE_REPO}"
                    }
                }

                String host
                if("${script.env.BRANCH_NAME}".toLowerCase().contains("verizon")){
                    host = "192.168.30.10"
                }else{
                    host = "192.168.30.50"
                }
                String url
                if (Constant.PLATFORM.toUpperCase().equals("DJANGO")) {
                    url = "http://${host}:3000"
                } else if (Constant.PLATFORM.toUpperCase().equals("MAGENTO2")) {
                    url = "http://${host}:4000"
                }else if(Constant.PLATFORM.toUpperCase().equals("DESIGN")){
                    url = "http://${host}:2000"
                }else if(Constant.PLATFORM.toUpperCase().equals("NODE")){
                    url = "http://${host}:8000"
                }

                //Send Success email with Deployment URL attached
                script.emailext attachLog: false, body: getDeploymentSuccessfulEmailContent("UAT Environment",
                        "${url}"), subject: '🔵 $PROJECT_NAME - Build # $BUILD_NUMBER - ' +
                        '$BUILD_STATUS', to: "${Notification.getDeveloperEmail()}, ${Notification.getBuildManagerEmail()}"

            }
        }
    }
}
