package com.contus.cd.stages

import com.contus.cd.helpers.BuildProperties
import com.contus.cd.helpers.JenkinsHelper
import com.contus.cd.helpers.Selenium
import com.contus.cd.helpers.SeleniumWeb
import com.contus.cd.Constant


/**
 * Automatic Test will triggered.
 */
public class BrowserTest extends AbstractStage {

	def SONAR_QUALITY_GATE_API = "${Constant.SONAR_HOST_URL}/api/qualitygates/project_status?format=json&projectKey="

	def EMAIL_TEMPLATE_REPO = "https://bitbucket.org/vplayed/jenkins-pipeline-email-templates"
	def EMAIL_TEMPLATE_BRANCH = "feature/phase1"

	def sonarProjectKey

	BrowserTest(Object script, JenkinsHelper jenkinsHelper) {
		super(script, 'Browser Test', jenkinsHelper)
	}

	/**
	 * Convert HTML file content to string
	 */
	String convertHTMLToString(def filename) {
		def emailContent = script.readFile encoding: 'UTF-8', file: "${filename}"
		return emailContent;
	}

	/**
	 * Retrieve's and prepare Deployment successful email content.
	 */
	String getAutoTestEmailContent(def id) {
		String fileName="$PROJECT_NAME"
		convertHTMLToString("templates/sonar_failed.html").replace("%MAIN_CONTENT%", 'The $PROJECT_NAME' +
				' of Build # $BUILD_NUMBER has been $BUILD_STATUS in SonarQube quality gate check, kindly fix it ASAP.')
				.replace("%SUB_CONTENT%", "URL: ${Constant.SONAR_HOST_URL}/overview?id=${id}")
				.replace("%TEAM%", "Build Team.");
	}

	@Override
	void execute() {
		script.stage(stageName) {
			script.node("Android-Build") {
				script.stage ('Browser Test') {
					SeleniumWeb seleniumWeb = new SeleniumWeb(script, jenkinsHelper)
					seleniumWeb.runTest("Mirrorfly_Chrome_Web_AutomationReport")
				}
				// script.stage ('Browser Test - Firefox') {
				// 	SeleniumWeb seleniumWeb = new SeleniumWeb(script, jenkinsHelper)
				// 	seleniumWeb.runTest("Mirrorfly_Firefox_Web_AutomationReport")
				// }
			}
		}
	}
}
