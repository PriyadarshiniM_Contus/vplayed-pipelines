package com.contus.cd.stages

import com.contus.cd.Constant
import com.contus.cd.helpers.JenkinsHelper

public class StaticAnalysisIOS extends AbstractStage {

    def SONAR_PROP_FILEPATH = "sonar-project.properties"

    StaticAnalysisIOS(Object script, JenkinsHelper jenkinsHelper) {
        super(script, 'Sonar', jenkinsHelper)
    }

    @Override
    void execute() {
        script.stage(stageName) {
            script.node("${Constant.NODE}") {

                /**
                 * Build display name
                 */
                script.currentBuild.displayName = "#${script.env.BUILD_NUMBER} StaticAnalysis"

                script.withEnv(["PATH=/User/jenkins/sonar-scanner/bin:/usr/local/Cellar/maven/3.6.1/bin:/usr/local/bin:/usr/local/sbin:/Users/jenkins/bin:/Library/Java/JavaVirtualMachines/jdk-11.0.4.jdk/Contents/Home/bin:/Users/jenkins/library/Android/sdk/tools:/Users/jenkins/library/Android/sdk/platform-tools:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/bin:/usr/local/bin:/usr/local/sbin:/Users/jenkins/bin:/Library/Java/JavaVirtualMachines/jdk-11.0.4.jdk/Contents/Home/bin:/Users/jenkins/library/Android/sdk/tools:/Users/jenkins/library/Android/sdk/platform-tools:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"]) {

                    /**
                    * Run SonarQube Scanner
                    */

                    def testResult = script.sh returnStdout: true, script: "build-wrapper-macosx-x86 --out-dir bw-output make clean all"
                    script.println("testResult: ${testResult}")

                    testResult = script.sh returnStdout: true, script: "/User/jenkins/sonar-scanner/bin/sonar-scanner " +
                            "-Dsonar.projectKey=Apptha_p078-contus-instantmessenger-iosv2.0 " +
                            "-Dsonar.organization=apptha " +
                            "-Dsonar.sources=. " +
                            "-Dsonar.cfamily.build-wrapper-output=bw-output " +
                            "-Dsonar.host.url=https://sonarcloud.io " +
                            "-Dsonar.login=0e5d8c765361169178ddd4b2efa0c7f84df0b995"
                    script.println("testResult: ${testResult}")
                }


                /**
                * Quality Gate Check
                * Set SonarQube Project Key before executing the stage
                */
                def qualityGateStage = new QualityGate(script, jenkinsHelper)
                qualityGateStage.setSonarProjectKey("0e5d8c765361169178ddd4b2efa0c7f84df0b995")
                qualityGateStage.execute();
            }
        }
    }

}
