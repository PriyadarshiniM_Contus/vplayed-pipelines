package com.contus.cd.stages

interface Stage extends Serializable {

    void execute()

}
