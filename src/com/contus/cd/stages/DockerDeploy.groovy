package com.contus.cd.stages

import com.contus.cd.Constant
import com.contus.cd.helpers.JenkinsHelper
import com.contus.cd.helpers.Notification
import com.contus.cd.stages.AbstractStage


public class DockerDeploy extends AbstractStage {

	DockerDeploy(Object script, JenkinsHelper jenkinsHelper) {
		super(script, 'Deploy Container', jenkinsHelper)
	}

	@Override
	void execute() {
		script.stage(stageName) {
			def PROJECT_REPO_BRANCH = "${script.env.BRANCH_NAME}"
			def tag_prefix
			if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.UAT_BRANCH) { 
				//Should be "stage"
				tag_prefix = 'uat-';
			} else if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.MASTER_BRANCH) { 
				//Should be "master"
				tag_prefix = 'production-';
			} else if (PROJECT_REPO_BRANCH.toLowerCase().contains(Constant.RELEASE_BRANCH)) { 
				//Should be "release"
				tag_prefix = 'qa-';
			} else if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.DEVELOP_BRANCH) { 
				//Should be "develop"
				tag_prefix = 'dev-';
			}

			script.node("${Constant.DEPLOY_NODE}") {
				// script.sh "docker pull ${Constant.DOCKER_REPO}/${Constant.DOCKER_IMAGE}:v${script.env.BUILD_NUMBER}"
				script.sh "docker container rm  ${Constant.DOCKER_CONTAINER} -f || true"

             if (Constant.PLATFORM.equals("node")) {
				script.sh "docker run --restart always -d  --name ${Constant.DOCKER_CONTAINER} --network=${Constant.DOCKER_NAMESPACE} ${Constant.DOCKER_REPO}/${Constant.DOCKER_IMAGE}:${tag_prefix}v${script.env.BUILD_NUMBER}"
			  } else  {
				script.sh "docker run -d  --name ${Constant.DOCKER_CONTAINER} --network=${Constant.DOCKER_NAMESPACE} ${Constant.DOCKER_REPO}/${Constant.DOCKER_IMAGE}:${tag_prefix}v${script.env.BUILD_NUMBER}"
			  }


				// Build Status
				script.currentBuild.result = "SUCCESS"

				// Sleep 5 Seconds to make sure the artifact are archived
				script.sleep 5;
				script.emailext attachmentsPattern: "", body: '$DEFAULT_CONTENT', recipientProviders:
				[
					[$class: 'CulpritsRecipientProvider'],
					[$class: 'DevelopersRecipientProvider'],
					[$class: 'RequesterRecipientProvider']
				], subject: '🔵 $PROJECT_NAME - Build # $BUILD_NUMBER - ' +
				'$BUILD_STATUS', to: "${Notification.getDeveloperEmail()}, ${Notification.getBuildManagerEmail()}"
			}
		}
	}
}
