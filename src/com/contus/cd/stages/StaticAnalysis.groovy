package com.contus.cd.stages

import com.contus.cd.Constant
import com.contus.cd.helpers.JenkinsHelper

public class StaticAnalysis extends AbstractStage {

    def SONAR_PROP_FILEPATH = "sonar-project.properties"

    StaticAnalysis(Object script, JenkinsHelper jenkinsHelper) {
        super(script, 'Sonar', jenkinsHelper)
    }

    def parseJSON(def json) {
        new groovy.json.JsonSlurperClassic().parseText(json)
    }

    /**
     * Convert HTML file content to string
     */
    String convertHTMLToString(def filename) {
        def emailContent = script.readFile encoding: 'UTF-8', file: "${filename}"
        return emailContent;
    }

    @Override
    void execute() {
        script.println("static stageName: ${stageName}")
        script.stage(stageName) {
            script.node("${Constant.NODE}") {

                /**
                 * Build display name
                 */
                script.currentBuild.displayName = "#${script.env.BUILD_NUMBER} StaticAnalysis"
                /**
                 * SonarQube project for each stage and branch
                 */
                def key = "";
                def BRANCH = "${script.env.BRANCH_NAME}"
                if (BRANCH.toLowerCase().contains("feature/")) { // Feature branch
                    key = BRANCH.replace("feature/", "");
                } else if (BRANCH.toLowerCase().contains("develop")) { // Develop branch
                    key = "develop"
                } else if (BRANCH.toLowerCase().contains("release/")) { // Release branch
                    key = BRANCH.replace("*/", "").replace("/", "-");
                }else{
                    key = BRANCH.replace("*/", "").replace("/", "-");
                }
				
				if (Constant.PLATFORM.equals("Java")) {
					script.sh("mvn clean install")
				}
                script.println("sonar prodproperties: ${SONAR_PROP_FILEPATH}")
                def sonarProperties = script.readFile encoding: 'UTF-8', file: "${SONAR_PROP_FILEPATH}"

                Properties propSonar = new Properties();
                // load a properties string
                propSonar.load(new StringReader(sonarProperties));

                def sonarProjectKey = propSonar.getProperty("sonar.projectKey") + "_" + key.toUpperCase();
                def sonarProjectName = propSonar.getProperty("sonar.projectName") + "-" + key;
                 script.println("sonar key: ${sonarProjectKey }")
                 script.println("sonar key: ${sonarProjectName}")
                /**
                 * Run SonarQube Scanner
                 */
                def sonarqubeScannerHome = script.tool name: 'SonarQubeScanner', type: 'hudson.plugins.sonar' +
                        '.SonarRunnerInstallation'
                script.sh "${sonarqubeScannerHome}/bin/sonar-scanner -Dproject.settings=${SONAR_PROP_FILEPATH} " +
                        "-Dsonar.projectKey=${sonarProjectKey} -Dsonar.projectName='${sonarProjectName}' -Dsonar.host.url=${Constant.SONAR_HOST_URL}"

                /**
                 * Quality Gate Check
                 * Set SonarQube Project Key before executing the stage
                 */
                def qualityGateStage = new QualityGate(script, jenkinsHelper)
                qualityGateStage.setSonarProjectKey(sonarProjectKey)
                qualityGateStage.execute();
            }
        }
    }

}
