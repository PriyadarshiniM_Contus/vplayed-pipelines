package com.contus.cd.stages

import com.contus.cd.Constant
import com.contus.cd.helpers.JenkinsHelper
import com.contus.cd.helpers.Utils

public class StaticAnalysisAndroid extends AbstractStage {

    def SONAR_PROP_FILEPATH = "sonar-project.properties"

    StaticAnalysisAndroid(Object script, JenkinsHelper jenkinsHelper) {
        super(script, 'Sonar', jenkinsHelper)
    }

    def parseJSON(def json) {
        new groovy.json.JsonSlurperClassic().parseText(json)
    }

    /**
     * Convert HTML file content to string
     */
    String convertHTMLToString(def filename) {
        def emailContent = script.readFile encoding: 'UTF-8', file: "${filename}"
        return emailContent;
    }

    void checkQAMailPropertiesFile(){
        def qamailProperties = script.readFile encoding: 'UTF-8', file: "qamail.properties"

        Properties propSonar = new Properties();
        // load a properties string
        propSonar.load(new StringReader(qamailProperties));

        ['PROJECT_ID', 'PROJECT_NAME', 'PLATFORM', 'SPRINT', 'COMPONENT', 'PHASE_NO', 'QA_TASK_ID', 'DEVELOPER_NAME',
         'BACKEND_URL', 'BACKEND_CREDENTIALS', 'FRONTEND_URL', 'PCR_LINK', 'NOTE'
        ].each { property ->
            if(propSonar.getProperty("${property}") == null){
                script.currentBuild.result = "FAILED"
                script.currentBuild.description = "Error qamail.properties"
                throw new hudson.AbortException("Property ${property} missing/incorrect in qamail.properties")
            }
        }
    }

    @Override
    void execute() {
        script.stage(stageName) {
            script.node("${Constant.NODE}") {

                /**
                 * SonarQube project for each stage and branch
                 */
                def key = "";
                def BRANCH = "${script.env.BRANCH_NAME}"
                if (BRANCH.toLowerCase().contains("feature/")) { // Feature branch
                    key = BRANCH.replace("feature/", "");
                } else if (BRANCH.toLowerCase().contains("develop")) { // Develop branch
                    key = "develop"
                } else if (BRANCH.toLowerCase().contains("release/")) { // Release branch
                    checkQAMailPropertiesFile()
                    key = BRANCH.replace("*/", "").replace("/", "-");
                }else{
                    key = BRANCH.replace("*/", "").replace("/", "-");
                }

                String parentFolder = ""
                if (Constant.FOLDERNAME.length() != 0) {
                    parentFolder = Constant.FOLDERNAME + "/"
                }
				
				script.println("${parentFolder}app/contus_internal_keystore.jks")

                //def checkFileExist = script.fileExists "${parentFolder}app/contus_internal_keystore.jks"
                // if (!checkFileExist) {
                //     Utils utils = new Utils(script)
                //     utils.copyFile("/home/jenkins/contus_internal_keystore.jks", "${parentFolder}app/")
                // }

                // Check and remove local.properties file
                def checkLocalPropsFileExist = script.fileExists "${parentFolder}local.properties"
                if(checkLocalPropsFileExist){
                    script.sh "rm -rf local.properties"
                }

                script.sh "chmod +x gradlew"
                script.sh "./gradlew clean assembleDebug"

                /**
                 * Build display name
                 */
                script.currentBuild.displayName = "#${script.env.BUILD_NUMBER} StaticAnalysis"


                def sonarProperties = script.readFile encoding: 'UTF-8', file: "${SONAR_PROP_FILEPATH}"

                Properties propSonar = new Properties();
                // load a properties string
                propSonar.load(new StringReader(sonarProperties));

                def sonarProjectKey = propSonar.getProperty("sonar.projectKey") + "_" + key.toUpperCase();
                def sonarProjectName = propSonar.getProperty("sonar.projectName") + "-" + key;

                /**
                 * Run SonarQube Scanner
                 */
                def sonarqubeScannerHome = script.tool name: 'SonarQubeScanner', type: 'hudson.plugins.sonar' +
                        '.SonarRunnerInstallation'
                script.sh "${sonarqubeScannerHome}/bin/sonar-scanner -Dproject.settings=${SONAR_PROP_FILEPATH} " +
                        "-Dsonar.projectKey=${sonarProjectKey} -Dsonar.projectName='${sonarProjectName}'"
                // -Dsonar.branch.name=${key} cant be used becasue to community edition

                /**
                 * Quality Gate Check
                 * Set SonarQube Project Key before executing the stage
                 */
                def qualityGateStage = new QualityGate(script, jenkinsHelper)
                qualityGateStage.setSonarProjectKey(sonarProjectKey)
                qualityGateStage.execute();
            }
        }
    }

}
