package com.contus.cd.stages

import com.contus.cd.Constant
import com.contus.cd.helpers.BuildProperties
import com.contus.cd.helpers.JenkinsHelper

/**
 * In this stage source code clone or checkout is done.
 * So this stage is called as "Preparation".
 * This is initial stage of the project.
 */
public class Preparation extends AbstractStage {

	Preparation(Object script, JenkinsHelper jenkinsHelper) {
		super(script, 'Preparation', jenkinsHelper)
	}

	@Override
	void execute() {
		def constant = new Constant()
		script.println("stageName: ${stageName}")
		script.stage(stageName) {
			script.println("NODE: ${Constant.NODE}")
			script.node("${Constant.NODE}") {
		

				//                File f = new File('test.txt')
				//                f.append("spec.job_url               = ${script.env.JOB_URL}\n")

				script.sh returnStdout: true, script:"echo ${Constant.SONAR_HOST_URL}"
				script.sh('#!/bin/sh -e\n' +"echo ${script.env.BRANCH_NAME}")

				//Checkout code
				script.checkout script.scm

				// Load build properties
				def buildProperties = new BuildProperties(script)
				buildProperties.readBuildProperties()
			}
			def PROJECT_REPO_BRANCH = "${script.env.BRANCH_NAME}"
			if (PROJECT_REPO_BRANCH.toLowerCase().contains(Constant.RELEASE_BRANCH) ) { //Should be "stage"
					Constant.setNODE("QA")
					script.node("${Constant.NODE}") {
		
					script.sh returnStdout: true, script:"echo ${Constant.SONAR_HOST_URL}"
					script.sh('#!/bin/sh -e\n' +"echo ${script.env.BRANCH_NAME}")

					//Checkout code
					script.checkout script.scm

					// Load build properties
					def buildProperties = new BuildProperties(script)
					buildProperties.readBuildProperties()
					Constant.setNODE("build-node")
				}
			} else if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.MASTER_BRANCH) { //Should be "master"
					Constant.setNODE("prodbuild")
					script.node("${Constant.NODE}") {
		
					script.sh returnStdout: true, script:"echo ${Constant.SONAR_HOST_URL}"
					script.sh('#!/bin/sh -e\n' +"echo ${script.env.BRANCH_NAME}")

					//Checkout code
					script.checkout script.scm

					// Load build properties
					def buildProperties = new BuildProperties(script)
					buildProperties.readBuildProperties()
					Constant.setNODE("build-node")
				}
			} else if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.UAT_BRANCH) {
				Constant.setNODE("UAT")
				script.node("${Constant.NODE}") {
	
					script.sh returnStdout: true, script:"echo ${Constant.SONAR_HOST_URL}"
					script.sh('#!/bin/sh -e\n' +"echo ${script.env.BRANCH_NAME}")

					//Checkout code
					script.checkout script.scm

					// Load build properties
					def buildProperties = new BuildProperties(script)
					buildProperties.readBuildProperties()
					Constant.setNODE("build-node")
				}
			} else if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.DEVELOP_BRANCH) {
				Constant.setNODE("DEV")
				script.node("${Constant.NODE}") {
	
					script.sh returnStdout: true, script:"echo ${Constant.SONAR_HOST_URL}"
					script.sh('#!/bin/sh -e\n' +"echo ${script.env.BRANCH_NAME}")

					//Checkout code
					script.checkout script.scm

					// Load build properties
					def buildProperties = new BuildProperties(script)
					buildProperties.readBuildProperties()
					Constant.setNODE("build-node")
				}
			}
		}
	}
}
