package com.contus.cd.stages

import com.contus.cd.Constant
import com.contus.cd.helpers.JenkinsHelper
import com.contus.cd.helpers.Notification

/**
 * The Quality Gate check stage.
 * {@see sonarProjectKey} to be set before executing the stage.
 *
 * The Sonarqube Quality Gate will be checked via API request.
 * If we get response as OK,then its considered as success else failure.
 * We shall set the jenkins build status accordingly to the response.
 */
public class QualityGate implements Serializable {

	def SONAR_QUALITY_GATE_API = "${Constant.SONAR_HOST_URL}/api/qualitygates/project_status?format=json&projectKey="

	def sonarProjectKey
	def script
	def jenkinsHelper

	QualityGate(Object script, JenkinsHelper jenkinsHelper) {
		this.script = script
		this.jenkinsHelper = jenkinsHelper
	}

	def parseJSON(def json) {
		new groovy.json.JsonSlurperClassic().parseText(json)
	}

	/**
	 * Convert HTML file content to string
	 */
	String convertHTMLToString(def filename) {
		def emailContent = script.readFile encoding: 'UTF-8', file: "${filename}"
		return emailContent;
	}

	/**
	 * Retrieve's and prepare Deployment successful email content.
	 */
	String getSonarFailedEmailContent(def id) {
		convertHTMLToString("templates/sonar_failed.html").replace("%MAIN_CONTENT%", 'The $PROJECT_NAME' +
				' of Build # $BUILD_NUMBER has been $BUILD_STATUS in SonarQube quality gate check, kindly fix it ASAP.')
				.replace("%SUB_CONTENT%", "URL: ${Constant.SONAR_HOST_URL}/dashboard?id=${id}")
				.replace("%TEAM%", "Build Team.");
	}

	void execute() {

		/**
		 * Quality Gate Check
		 */
		def response = ""
		//Sleep 5 seconds and wait for SonarQube to process report for us
		script.sleep 10;

		response = script.httpRequest authentication: 'SonarQubeScannerUNPW', acceptType: 'APPLICATION_JSON', contentType: 'APPLICATION_JSON',
		url: "${SONAR_QUALITY_GATE_API}+${sonarProjectKey}"

		//script.println('Response: '+response.content)
		def json = parseJSON(response.content)

		def status = json['projectStatus']['status'].toString()
		def str2 = "ok"
		if (str2.toUpperCase() == status.toUpperCase()) {
			script.println("GREEN")
			script.currentBuild.result = "SUCCESS"
		} else {
			script.println("RED")
			script.println("SonarQube Check FAILED")
			script.currentBuild.result = "FAILED"
			script.currentBuild.description = "Quality Gate Failed"

			/**
			 * Checkout the Email Template files
			 */
			script.sh 'mkdir -p templates'
			script.dir("templates") {
			script.git branch: "${Constant.EMAIL_TEMPLATE_BRANCH}", credentialsId: "${Constant.CREDENTIAL}",
				url: "${Constant.EMAIL_TEMPLATE_REPO}"
			}

			//Send failure email with Sonarqube link attached
			script.emailext attachLog: false, body: getSonarFailedEmailContent("${sonarProjectKey}"),
			subject: '🔴 $PROJECT_NAME - Build # $BUILD_NUMBER - Static Analysis $BUILD_STATUS', 
			recipientProviders:
			[
				[$class: 'CulpritsRecipientProvider'],
				[$class: 'DevelopersRecipientProvider'],
				[$class: 'RequesterRecipientProvider']
			],
			to: "${Notification.getDeveloperEmail()}, ${Notification.getBuildManagerEmail()}"
		
			throw new hudson.AbortException("SonarQube Check FAILED")
		}
	}
}
