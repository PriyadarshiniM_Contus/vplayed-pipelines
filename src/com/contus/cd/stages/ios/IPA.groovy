package com.contus.cd.stages.ios

import com.contus.cd.Constant
import com.contus.cd.helpers.JenkinsHelper
import com.contus.cd.helpers.Utils

/**
 * Build IPA and rename it according to project and build.
 * Archive IPA.
 *
 * @author sathishkumar@contus.in
 * @version 1.0
 */
public class IPA implements Serializable {

    def script
    def jenkinsHelper

    IPA(Object script, JenkinsHelper jenkinsHelper) {
        this.script = script
        this.jenkinsHelper = jenkinsHelper
    }

    /**
     * Gradle clean.
     * Gradle Assemble Debug.
     * Rename the IPA with build number.
     * Archive IPA (Jenkins Artifacts).
     */
    String build(def env, def flavor) {
        if (Constant.XCODE_PROJECT_NAME.length() != 0) {

            // Bind Build Number and Date
            Utils utils = new Utils(script)
            utils.writeBuildNumberDateToiOSApp()

            //utils.copyFile("releasenote.txt", "${Constant.XCODE_PROJECT_NAME}/${Constant.XCODE_PROJECT_NAME}/")
            //utils.moveFile("versioninfo.txt", "${Constant.XCODE_PROJECT_NAME}/${Constant.XCODE_PROJECT_NAME}/")

            String parentFolder = ""
            if (Constant.FOLDERNAME.length() != 0) {
                parentFolder = Constant.FOLDERNAME + "/"
            }

            // Remove old builds
            script.sh "rm -rf build/${Constant.XCODE_PROJECT_NAME}/*"

            // Unlock the login keychain
            script.sh "security set-key-partition-list -S apple-tool:,apple:,codesign: -s -k welcome@123 login.keychain-db"

            if (Constant.FOLDERNAME.length() != 0) {
                script.dir("${Constant.FOLDERNAME}") {
                    script.sh "/usr/local/bin/pod install"
                }
            }else{
                script.sh "/usr/local/bin/pod install"
            }


            String environment = "${env}".toLowerCase().tokenize().collect { it.capitalize() }.join('')

            // Clean and Archive the IPA
            script.sh "xcodebuild -scheme ${environment} -workspace ${parentFolder}${Constant.XCODE_PROJECT_NAME}.xcworkspace " +
                    "clean archive -archivePath build/${Constant.XCODE_PROJECT_NAME}"
            // Sign and Export the IPA
            script.sh "xcodebuild -exportArchive -exportOptionsPlist ${parentFolder}${Constant.XCODE_PROJECT_NAME}" +
                    ".plist -archivePath build/${Constant.XCODE_PROJECT_NAME}.xcarchive -exportPath build/${Constant.XCODE_PROJECT_NAME} PROVISIONING_PROFILE_SPECIFIER = \"${Constant.PROVISIONING_PROFILE}\""

            // Rename the IPA accordingly
            script.sh "mv build/${Constant.XCODE_PROJECT_NAME}/${environment}.ipa " +
                    "build/${Constant.XCODE_PROJECT_NAME}/${Constant.APP_NAME.replaceAll("\\s", "")}_${environment}_${script.env.BUILD_NUMBER}.ipa"

            def attachment = "**/*${script.env.BUILD_NUMBER}.ipa"

            // Archive the IPA
            script.archiveArtifacts "${attachment}"
            return "${attachment}"

        } else {
            script.currentBuild.result = "FAILED"
            script.currentBuild.description = "Xcode Project Name cannot be empty"
            throw new hudson.AbortException("Xcode Project Name cannot be empty")
        }
    }
}
