package com.contus.cd.stages.ios

import com.contus.cd.Constant
import com.contus.cd.helpers.JenkinsHelper
import com.contus.cd.helpers.Notification
import com.contus.cd.stages.AbstractStage

public class StaticAnalysisIos extends AbstractStage {

    def EMAIL_TEMPLATE_REPO = "https://bitbucket.org/vplayed/jenkins-pipeline-email-templates"
    def EMAIL_TEMPLATE_BRANCH = "feature/phase1"

    StaticAnalysisIos(Object script, JenkinsHelper jenkinsHelper) {
        super(script, 'Sonar', jenkinsHelper)
    }

    def parseJSON(def json) {
        new groovy.json.JsonSlurperClassic().parseText(json)
    }

    /**
     * Convert HTML file content to string
     */
    String convertHTMLToString(def filename) {
        def emailContent = script.readFile encoding: 'UTF-8', file: "${filename}"
        return emailContent;
    }

    /**
     * Retrieve's and prepare Deployment successful email content.
     */
    String getEmailContent() {
        convertHTMLToString("templates/email_template.html").replace("%MAIN_CONTENT%", 'The $PROJECT_NAME' +
                ' of Build # $BUILD_NUMBER has been $BUILD_STATUS in SwiftLint check, kindly fix it ASAP.')
                .replace("%SUB_CONTENT%", "Report URL: ${script.env.JOB_URL}${script.env.BUILD_NUMBER}/checkstyleResult")
                .replace("%TEAM%", "Build Team.");
    }

    /**
     * Prepare exclusion for Swift Lint
     * @return exclusion string
     */
    String prepareExclusions() {
        def defaultExclusions = "  - ${Constant.XCODE_PROJECT_NAME}/Carthage\n" +
                "  - ${Constant.XCODE_PROJECT_NAME}/Pods\n"
        def otherExclusions = ""

        // Check properties file exists
        def checkFileExist = script.fileExists "${Constant.SWIFTLINT_PROPERTIES}"
        if (checkFileExist) {
            String swiftLintProperties = script.readFile encoding: 'UTF-8', file: "${Constant.SWIFTLINT_PROPERTIES}"

            Properties propSwiftLint = new Properties();
            // load a properties string
            propSwiftLint.load(new StringReader(swiftLintProperties));

            String[] excluded = propSwiftLint.getProperty("swiftlint.exclusions").split(',')

            for (String value : excluded) {
                otherExclusions += "  - ${value.trim()}\n"
            }
        }

        return "${defaultExclusions}${otherExclusions}"
    }

    @Override
    void execute() {
        script.stage(stageName) {
            script.node("${Constant.NODE}") {
                if ("${Constant.LANGUAGE}".toLowerCase().contains("swift")) {

                    //Prepare exclusions
                    String exclusions = prepareExclusions()

                    script.sh "echo 'whitelist_rules:\n" +
                            "excluded: # paths to ignore during linting. Takes precedence over `included`.\n" +
                            "${exclusions}" +
                            "# rules that have both warning and error levels, can set just the warning level\n" +
                            "# implicitly\n" +
                            "line_length: 150\n" +
                            "# they can set both implicitly with an array\n" +
                            "reporter: \"checkstyle\" # reporter type (xcode, json, csv, checkstyle)' > .swiftlint.yml"

                    boolean lint = script.sh returnStdout: true, script: "/usr/local/bin/swiftlint lint --reporter " +
                            "checkstyle > report.xml || true"

                    String report = script.readFile encoding: 'UTF-8', file: "report.xml"

                    if (report.contains("<file")) {
                        lint = false
                    } else {
                        lint = true
                    }
                    script.println("Lint: ${lint}")

                    script.checkstyle canComputeNew: false, defaultEncoding: 'UTF-8', healthy: '', pattern: 'report.xml', unHealthy: ''

                    if (!lint) {
                        script.currentBuild.result = "FAILED"
                        script.currentBuild.description = "Static Analysis Swift lint Failed"

                        /**
                         * Checkout the Email Template files
                         */
                        script.sh 'mkdir -p templates'
                        script.dir("templates") {
                            script.git branch: "${EMAIL_TEMPLATE_BRANCH}", credentialsId: "${Constant.CREDENTIAL}",
                                    url: "${EMAIL_TEMPLATE_REPO}"
                        }

                        // Send failure email with Sonarqube link attached
                        script.emailext body: getEmailContent(),
                                subject: '🔴 $PROJECT_NAME - Build # $BUILD_NUMBER - Static Analysis $BUILD_STATUS', recipientProviders:
                                [[$class: 'CulpritsRecipientProvider'], [$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']],
                                to: "${Notification.getDeveloperEmail()}, ${Notification.getBuildManagerEmail()}"

                        script.error "Static Analysis Swift lint Failed"
                    }
                }
            }
        }
    }

}
