package com.contus.cd

class Constant implements Serializable {

	static String CREDENTIAL = "f4de447e-8ca2-48d8-b558-b774a2e6cfba"
	static String NODE = ""
	static String SONAR_HOST_URL = "https://sonar-vplayed.contus.us"

	static String SELENIUM_REPO_URL = "https://bitbucket.org/Apptha/p078-contus-instantmessenger-automation-and"
	static String SELENIUM_REPO_BRANCH = "demo"

	static String IOS_SELENIUM_REPO_URL = "https://bitbucket.org/Apptha/p078-contus-instantmessenger-automation-iOS"
	static String IOS_SELENIUM_REPO_BRANCH = "feature/feature_automation"

	static String SELENIUM_WEB_REPO_URL = "https://bitbucket.org/Apptha/p078-contus-instantmessenger-automation-webchat"
	static String SELENIUM_WEB_REPO_BRANCH = "feature/ContusFlyWebAutomation"

	static String API_AUTOMATION_REPO_URL = "https://bitbucket.org/Apptha/p078-contus-instantmessenger-api-testing"
	static String API_AUTOMATION_REPO_BRANCH = "feature/test_rest_api"

	static String PLATFORM = ""
	static String APP_NAME = ""
	static String APK_NAME = ""
	static String DEV_APK_NAME = ""
	static String UAT_APK_NAME = ""
	static String QA_APK_NAME = ""

	static String MODULE_NAME = ""
	static String XCODE_PROJECT_NAME = ""
	static String LANGUAGE = ""
	static String FOLDERNAME = ""
	static String PROVISIONING_PROFILE = "Contus_Inhouse_Certificate_2017"

	static String SWIFTLINT_PROPERTIES = "swiftlint.properties"

	static String DEV_IPA=  ""
	static String QA_IPA=  ""
	static String IPA=  ""
	static String UAT_IPA=  ""
	static String DEV_IPA_PROFILE=  ""
	static String QA_IPA_PROFILE=  ""
	static String IPA_PROFILE=  ""
	static String UAT_IPA_PROFILE=  ""
	static String DOCKER_REPO=  ""
	static String DOCKER_IMAGE=  ""
	static String DOCKER_CONTAINER=  ""
	static String DEPLOY_NODE=  ""
	static String DOCKER_NAMESPACE= ""
	static String DOCKER_BUILDCONTAINER=  ""
	static String DOCKER_SERVICE=  ""
	static String DOCKER_BRANCH= ""
	static String DOCKER_USERNAME= "admin"
	static String DOCKER_PASSWORD= "vplayed#321sn"
	static String ENV = ""
	static String RUN_AUTO_TEST = ""
	static Boolean AUTOMATION = false
	static Boolean FIREBASE_DISTRIBUTION = false
	static String DEVELOP_BRANCH = "develop"
	static String RELEASE_BRANCH = "release/"
	static String MASTER_BRANCH = "master"
	static String UAT_BRANCH = "stage"
	static String FIREBASE_DISTRIBUTION_URL = ""
	static String AWSECR_CREDENTIAL = "ecr:ap-south-1:aws-ecr-login"
	static String EMAIL_TEMPLATE_REPO = "https://gitlab.com/Vplayed/training/jenkins-pipeline-email-templates"
	static String EMAIL_TEMPLATE_BRANCH = "master"
	Constant() {}
}

