package com.contus.cd

import com.contus.cd.helpers.Exception
import com.contus.cd.helpers.JenkinsHelper
import com.contus.cd.stages.*
import com.contus.cd.stages.ios.StaticAnalysisIos
import org.jenkinsci.plugins.workflow.cps.DSL
import com.contus.cd.helpers.EmailContent

/**
 * Pipeline for Jenkins2 Pipeline
 *
 * @author sathishkumar@contus.in
 * @version 1.0
 */
class Pipeline implements Serializable {

	def script

	def stages = []

	DSL steps

	JenkinsHelper jenkinsHelper

	static boolean isAndroid = false

	static boolean isIOS = false

	static boolean isDebug = false

	static builder(script, DSL steps) {
		return new Builder(script, steps)
	}

	static class Builder implements Serializable {

		// Stages of pipeline
		def stages = []

		// Script object to execute the jenkins based scripts
		def script

		DSL steps

		// {@link JenkinsHelper}
		JenkinsHelper jenkinsHelper

		//Project repo branch name. Used to select stages to be executed which changes according to branch
		def PROJECT_REPO_BRANCH;

		Builder(def script, DSL steps) {
			this.script = script
			this.steps = steps
			this.jenkinsHelper = new JenkinsHelper(script)
			this.PROJECT_REPO_BRANCH = "${this.script.env.BRANCH_NAME}"
		}

		def withPreparationStage() {
			stages << new Preparation(script, jenkinsHelper)
			return this
		}

		def withStaticAnalysisStage() {
			stages << new StaticAnalysis(script, jenkinsHelper)
			return this
		}

		def withStaticAnalysisAndroidStage() {
			stages << new StaticAnalysisAndroid(script, jenkinsHelper)
			return this
		}

		def withStaticAnalysisIOSStage() {
			stages << new StaticAnalysisIOS(script, jenkinsHelper)
			return this
		}

		def withBuildDevStage() {
			stages << new BuildDev(script, jenkinsHelper)
			return this
		}

		def withBuildQAStage() {
			stages << new BuildQA(script, jenkinsHelper)
			return this
		}

		def withBuildUATStage() {
			stages << new BuildUAT(script, jenkinsHelper)
			return this
		}

		def withBuildMasterStage() {
			stages << new BuildMaster(script, jenkinsHelper)
			return this
		}

		def withBuildFastLaneStage() {
			stages << new FastLane(script, jenkinsHelper)
			return this
		}


		def withBuildDocker() {
			stages << new DockerBuild(script, jenkinsHelper)
			return this
		}

		def withDeployDocker() {
			stages << new DockerDeploy(script, jenkinsHelper)
			return this
		}

		def withDeployDevStage() {
			stages << new DeployDev(script, jenkinsHelper)
			return this
		}

		def withDeployUatStage() {
			stages << new DeployUAT(script, jenkinsHelper)
			return this
		}

		def withDeployQAStage() {
			stages << new DeployQA(script, jenkinsHelper)
			return this
		}

		def withAutoTestStage() {
			stages << new AutoTest(script, jenkinsHelper)
			return this
		}

		def withBrowserTestStage() {
			stages << new BrowserTest(script, jenkinsHelper)
			return this
		}

		def withJMeterStage() {
			stages << new JMeterTest(script, jenkinsHelper)
			return this
		}

		def build() {
			return new Pipeline(this)
		}

		def buildWebDockerPipeline(Boolean runAutomationTest = false, Boolean dockerDeploy = false) {
			withPreparationStage()
			Constant.PLATFORM = "node"
			//withStaticAnalysisStage()
			if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.DEVELOP_BRANCH) { //Should be "develop"
				Constant.DEPLOY_NODE="Vplayed-Dev"
				// Check if docker deployment is requested
				if(dockerDeploy)
				{
					withBuildDocker()
					withDeployDocker()
				}
				//withBrowserTestStage()
			}
			if (PROJECT_REPO_BRANCH.toLowerCase().contains("release")) { //Should be "develop"
				Constant.DEPLOY_NODE="Vplayed-Qa"
				// Check if docker deployment is requested
				if(dockerDeploy)
				{
					withBuildDocker()
					withDeployDocker()
				}
				// Check if the automation run is requested from repo
				if(runAutomationTest) {
					withBrowserTestStage()
				}
				withBuildQAStage()
			}
			return new Pipeline(this)
		}

		def buildWebPipeline() {
			withPreparationStage()
			Constant.PLATFORM = "Web"
			withStaticAnalysisStage()
			if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.DEVELOP_BRANCH) { //Should be "develop"
				// Do nothing
			}
			if (PROJECT_REPO_BRANCH.toLowerCase().contains("release")) { //Should be "develop"
				// Do nothing
			}
			return new Pipeline(this)
		}

		def buildWebSDKDockerPipeline() {
			withPreparationStage()
			Constant.PLATFORM = "Web"
			withStaticAnalysisStage()
			if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.DEVELOP_BRANCH) { //Should be "develop"
				Constant.DEPLOY_NODE="Vplayed-Dev"
				//wittBuildDocker()
				//withDeployDocker()
				//withBrowserTestStage()
			}
			if (PROJECT_REPO_BRANCH.toLowerCase().contains("release")) { //Should be "develop"
				Constant.DEPLOY_NODE="Vplayed-Qa"
				//withBuildDocker()
				//withDeployDocker()
				//withBrowserTestStage()
				withBuildQAStage()
			}
			return new Pipeline(this)
		}

		def buildPhpDockerPipeline() {
			
			Constant.PLATFORM = "Php"
			if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.DEVELOP_BRANCH) { //Should be "develop"
			    Constant.setNODE("build-node")
				withPreparationStage()
				Constant.DOCKER_BRANCH = "develop"
				withBuildDocker()
				Constant.DEPLOY_NODE="DEV"
				withDeployDocker()
			}
		    else if (PROJECT_REPO_BRANCH.toLowerCase().contains(Constant.RELEASE_BRANCH)) { //Should be "release"
			    Constant.setNODE("QA")
				withPreparationStage()
				withStaticAnalysisStage()
				Constant.DOCKER_BRANCH = "release"
				withBuildDocker()
				Constant.DEPLOY_NODE="QA"
				withDeployDocker()
			}
			else if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.UAT_BRANCH) { //Should be "stage"
			    Constant.setNODE("UAT")
				withPreparationStage()
				Constant.DOCKER_BRANCH = "stage"
				withBuildDocker()
				Constant.DEPLOY_NODE="UAT"
				withDeployDocker()
			}
			else if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.MASTER_BRANCH) { //Should be "master"
			    Constant.setNODE("prodbuild")
				withPreparationStage()
				withStaticAnalysisStage()
				Constant.DOCKER_BRANCH = "master"
				withBuildDocker()
				Constant.DEPLOY_NODE="prodbuild"
				withDeployDocker()				
			}else{
				 withPreparationStage()
				 withStaticAnalysisStage()
			}
			
			return new Pipeline(this)
		}

		def buildAngularDockerPipeline() {
			Constant.PLATFORM = "angular"
			if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.DEVELOP_BRANCH) { //Should be "develop"
			    Constant.setNODE("build-node")
				withPreparationStage()
				Constant.DOCKER_BRANCH = "develop"
				withBuildDocker()
				Constant.DEPLOY_NODE="DEV"
				withDeployDocker()
			}
		    else if (PROJECT_REPO_BRANCH.toLowerCase().contains(Constant.RELEASE_BRANCH)) { //Should be "release"
			    Constant.setNODE("QA")
				withPreparationStage()
				withStaticAnalysisStage()
				Constant.DOCKER_BRANCH = "release"
				withBuildDocker()
				Constant.DEPLOY_NODE="QA"
				withDeployDocker()
			}
			else if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.UAT_BRANCH) { //Should be "stage"
			    Constant.setNODE("UAT")
				withPreparationStage()
				Constant.DOCKER_BRANCH = "stage"
				withBuildDocker()
				Constant.DEPLOY_NODE="UAT"
				withDeployDocker()
			}
			else if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.MASTER_BRANCH) { //Should be "master"
			    Constant.setNODE("prodbuild")
				withPreparationStage()
				withStaticAnalysisStage()
				Constant.DOCKER_BRANCH = "master"
				withBuildDocker()
				Constant.DEPLOY_NODE="prodbuild"
				withDeployDocker()				
			}else{
				 withPreparationStage()
				 withStaticAnalysisStage()
			}
			return new Pipeline(this)
		}

		def buildNodeDockerPipeline() {
			Constant.PLATFORM = "node"
			if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.DEVELOP_BRANCH) { //Should be "develop"
			    Constant.setNODE("build-node")
				withPreparationStage()
				Constant.DOCKER_BRANCH = "develop"
				withBuildDocker()
				Constant.DEPLOY_NODE="DEV"
				withDeployDocker()
			}
		    else if (PROJECT_REPO_BRANCH.toLowerCase().contains(Constant.RELEASE_BRANCH)) { //Should be "release"
			    Constant.setNODE("QA")
				withPreparationStage()
				withStaticAnalysisStage()
				Constant.DOCKER_BRANCH = "release"
				withBuildDocker()
				Constant.DEPLOY_NODE="QA"
				withDeployDocker()
			}
			else if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.UAT_BRANCH) { //Should be "stage"
			    Constant.setNODE("UAT")
				withPreparationStage()
				Constant.DOCKER_BRANCH = "stage"
				withBuildDocker()
				Constant.DEPLOY_NODE="UAT"
				withDeployDocker()
			}
			else if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.MASTER_BRANCH) { //Should be "master"
			    Constant.setNODE("prodbuild")
				withPreparationStage()
				withStaticAnalysisStage()
				Constant.DOCKER_BRANCH = "master"
				withBuildDocker()
				Constant.DEPLOY_NODE="prodbuild"
				withDeployDocker()				
			}else{
				 withPreparationStage()
				 withStaticAnalysisStage()
			}
			return new Pipeline(this)
		}

		def buildJavaDockerPipeline() {
			
			Constant.PLATFORM = "Java"
			//withStaticAnalysisStage()
			if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.DEVELOP_BRANCH) { //Should be "develop"
			    Constant.setNODE("build-node")
			    withPreparationStage()
		     	Constant.DOCKER_BRANCH = "develop"
				withBuildDocker()
			 	Constant.DEPLOY_NODE="Vplayed-Dev"
				withDeployDocker()
			}
			else if (PROJECT_REPO_BRANCH.toLowerCase().contains(Constant.RELEASE_BRANCH)) { //Should be "release"
			    Constant.setNODE("uatbuild")
				withPreparationStage()
				//withStaticAnalysisStage()
				Constant.DOCKER_BRANCH = "release"
				withBuildDocker()
			}
			else if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.UAT_BRANCH) { //Should be "stage"
			    Constant.setNODE("uatbuild")
				withPreparationStage()
				//withStaticAnalysisStage()
				Constant.DOCKER_BRANCH = "stage"
				withBuildDocker()
				
				
			}
			else if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.MASTER_BRANCH) { //Should be "master"
			    Constant.setNODE("prodbuild")
				withPreparationStage()
				//withStaticAnalysisStage()
				Constant.DOCKER_BRANCH = "master"
				withBuildDocker()
				
				
			}else{
				 withPreparationStage()
				 withStaticAnalysisStage()
			}
			return new Pipeline(this)
		}

		def buildDefaultAndroidPipeline(Boolean runAutomationTest = false, Boolean runFastlane = false) {
			setIsAndroid(true)
			withPreparationStage()
			Constant.PLATFORM = "Android"
			Constant.AUTOMATION = runAutomationTest
			withStaticAnalysisAndroidStage()
			if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.DEVELOP_BRANCH) {
				//withBuildDevStage()
			}

			if (PROJECT_REPO_BRANCH.toLowerCase().contains(Constant.RELEASE_BRANCH)) {
				// if(runAutomationTest) {
				// 	withAutoTestStage()
				// }
				// withBuildQAStage()
			}

			if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.MASTER_BRANCH) {
				// if(runFastlane) {
				// 	withBuildFastLaneStage()
				// } else {
				// 	withBuildMasterStage()
				// }
			}

			return new Pipeline(this)
		}

		def buildFastlaneAndroidPipeline() {
			// To make sure fastlane for Android runs in mac server
			setIsIOS(true)
			withPreparationStage()
			Constant.PLATFORM = "Android"
			withBuildFastLaneStage()

			return new Pipeline(this)
		}

		def buildDefaultIOSPipeline(Boolean runAutomationTest = false, Boolean runFastlane = false) {
			setIsIOS(true)
			withPreparationStage()
			Constant.PLATFORM = "iOS"

			if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.DEVELOP_BRANCH) {
				//withStaticAnalysisIOSStage()
				withBuildDevStage()
			}
			if (PROJECT_REPO_BRANCH.toLowerCase().contains(Constant.RELEASE_BRANCH)) {
				if(runAutomationTest) {
					withAutoTestStage()
				}
				withBuildQAStage()
			}

			if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.MASTER_BRANCH) {
				if(runFastlane) {
					withBuildFastLaneStage()
				} else {
					withBuildMasterStage()
				}
			}

			if (PROJECT_REPO_BRANCH.toLowerCase() == Constant.UAT_BRANCH) {
				withBuildUATStage()
			}

			return new Pipeline(this)
		}

		def buildDefaultIOSFastLanePipeline() {
			setIsIOS(true)
			withPreparationStage()
			Constant.PLATFORM = "iOS"
			withBuildFastLaneStage()

			return new Pipeline(this)
		}


		def buildAutoTestPipeline() {
			withAutoTestStage()
			return new Pipeline(this)
		}

		def buildAPITestPipeline() {
			withJMeterStage()
			return new Pipeline(this)
		}
	}

	private Pipeline(Builder builder) {
		this.script = builder.script
		this.stages = builder.stages
		this.steps = builder.steps
		this.jenkinsHelper = builder.jenkinsHelper
	}

	void execute() {

		// Choose the node suitable for branch before execution
		chooseNode()

		// `stages.each { ... }` does not work, see https://issues.jenkins-ci.org/browse/JENKINS-26481
		for (Stage stage : stages) {

			try {
				stage.execute()
			} catch (err) {
				new Exception(script).handle(err)
			}
		}
	}

	void chooseNode() {
		def PROJECT_REPO_BRANCH = "${script.env.BRANCH_NAME}"
		script.println("isAndroid: ${isAndroid}")
		if (isAndroid) {
			Constant.setNODE("Android-Build")
		} else if (isIOS) {
			Constant.setNODE("build-node")
		} else {
			Constant.setNODE("build-node")
		}
	}
}