package com.contus.cd.helpers
/**
 * Requirement check for all builds.
 * 1. Check if the qamail.properties, releasenote.txt file exist before QA build starts
 *
 * @author sathishkumar@contus.in
 * @version 1.0
 */
class Utils implements Serializable {

    def script
    def BUILD_PROP_FILEPATH = "build.properties"

    Utils() {}

    Utils(Object script) {
        this.script = script
    }

    void isQaMailProperties() {
        def checkFileExist = script.fileExists 'qamail.properties'
        if (!checkFileExist) {
            script.currentBuild.result = "FAILED"
            script.currentBuild.description = "qamail.properties file not found"
            throw new hudson.AbortException("qamail.properties file not found")
        }
    }

    void isReleaseNote() {
        def checkFileExist = script.fileExists 'releasenote.txt'
        if (!checkFileExist) {
            script.currentBuild.result = "FAILED"
            script.currentBuild.description = "qamail.properties file not found"
            throw new hudson.AbortException("qamail.properties file not found")
        }
    }

    /**
     * Bind Build Number and Date in the releasenote.txt file
     */
    void writeBuildNumberDateToReleaseNote() {
        def date = getDate()
        script.sh "sed -i \"s|#BUILD_NO#|Build Number: #${script.env.BUILD_NUMBER}|g\" releasenote.txt"
        script.sh "sed -i \"s|#BUILD_DATE#|Date: ${date}|g\" releasenote.txt"
    }

    /**
     * Bind Build Number and Date in the phtml file
     */
    void writeBuildNumberDateMagentoFooter() {
        def date = getDate()
        script.sh "sed -i \"s|#BUILD_NO#|${script.env.BUILD_NUMBER}|g\" copyright.phtml"
        script.sh "sed -i \"s|#BUILD_DATE#|${date}|g\" copyright.phtml"
    }

    /**
     * Bind Build Number and Date in the phtml file
     */
    void writeBuildNumberDateAdminDesignFooter() {
        def date = getDate()
        script.sh "sed -i \"s|#BUILD_NO#|${script.env.BUILD_NUMBER}|g\" bottom-footer.php"
        script.sh "sed -i \"s|#BUILD_DATE#|${date}|g\" bottom-footer.php"

        script.sh "sed -i \"s|#BUILD_NO#|${script.env.BUILD_NUMBER}|g\" monitor-bottom-footer.php"
        script.sh "sed -i \"s|#BUILD_DATE#|${date}|g\" monitor-bottom-footer.php"
    }

    /**
     * Bind Build Number and Date in the versioninfo.txt file
     */
    void writeBuildNumberDateToiOSApp() {
        def date = getDate()
//        script.sh "sed \"s|#BUILD_NO#|Build Number: #${script.env.BUILD_NUMBER}|g\" releasenote.txt"
//        script.sh "sed \"s|#BUILD_DATE#|Date: ${date}|g\" releasenote.txt"

        script.sh "find . -type f -name \"releasenote.txt\" | xargs sed -i '' 's/#BUILD_NO#/Build Number: #${script.env.BUILD_NUMBER}/g'"
        script.sh "find . -type f -name \"releasenote.txt\" | xargs sed -i '' 's/#BUILD_DATE#/Date: ${date}/g'"

        //script.sh "sed \"s|#BUILD_NO#|${script.env.BUILD_NUMBER}|g\" versioninfo.txt"
        //script.sh "sed \"s|#BUILD_DATE#|${date}|g\" versioninfo.txt"
    }

    /**
     * Bind Build Number and Date in the versioninfo.txt file
     */
    void writeBuildNumberDateToAndroidApp() {
        def date = getDate()
        script.sh "sed -i \"s|#BUILD_NO#|${script.env.BUILD_NUMBER}|g\" build.properties"
        script.sh "sed -i \"s|#BUILD_DATE#|${date}|g\" build.properties"
    }

    /**
     * Get the current date
     * @return def Date
     */
    public def getDate() {
        def date = new Date()
        return date.format("MMMMM d, Y")
    }

    void moveFile(String fileName, String path) {
        script.sh "mv ${fileName} ${path}"
    }

    void copyFile(String fileName, String path) {
        script.sh "cp -r ${fileName} ${path}"
    }
}

