package com.contus.cd.helpers

/**
 * Exception handling class.
 * Set the build status to FAILED
 * Send email to author
 *
 * @author sathishkumar@contus.in
 * @version 1.0
 */
class Exception implements Serializable {

    def script

    Exception(script) {
        this.script = script
    }

    void handle(def err) {
        script.currentBuild.result = "FAILURE"

        //Send failure email without build log attached
//        script.emailext attachLog: false, body: '$DEFAULT_CONTENT', subject: '🔴 $PROJECT_NAME' +
//                ' - Build # $BUILD_NUMBER - $BUILD_STATUS', to: "${Notification.getDeveloperEmail()}, ${Notification.getBuildManagerEmail()}"

        script.error "Build failed: ${err.toString()}"


    }

}
