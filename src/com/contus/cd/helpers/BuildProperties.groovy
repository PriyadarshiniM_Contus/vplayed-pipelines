package com.contus.cd.helpers

import com.contus.cd.Constant

/**
 * Retrieve properties value from the build.properties file, and store it as Constant.
 *
 * @author sathishkumar@contus.in
 * @version 1.0
 */
class BuildProperties implements Serializable {

	def script
	def BUILD_PROP_FILEPATH = "build.properties"

	BuildProperties() {}

	BuildProperties(Object script) {
		this.script = script
	}

	/**
	 * Read the build properties
	 */
	void readBuildProperties() {
		def buildProperties = script.readFile encoding: 'UTF-8', file: "${BUILD_PROP_FILEPATH}"

		Properties propBuild = new Properties();
		// load a properties string
		propBuild.load(new StringReader(buildProperties));

		Constant.APP_NAME = propBuild.getProperty("build.app.name")
		Constant.MODULE_NAME = propBuild.getProperty("build.app.moduleName")
		script.println("APP_NAME: ${Constant.APP_NAME}")
		script.println("MODULE_NAME: ${Constant.MODULE_NAME}")
		if(propBuild.getProperty("build.app.platform") !=  null){
			Constant.PLATFORM = propBuild.getProperty("build.app.platform")
		}
		if(propBuild.getProperty("build.apk.name") !=  null){
			Constant.APK_NAME = propBuild.getProperty("build.apk.name")
		}
		if(propBuild.getProperty("build.dev.apk.name") !=  null){
			Constant.DEV_APK_NAME = propBuild.getProperty("build.dev.apk.name")
		}
		if(propBuild.getProperty("build.uat.apk.name") !=  null){
			Constant.UAT_APK_NAME = propBuild.getProperty("build.uat.apk.name")
		}
		if(propBuild.getProperty("build.qa.apk.name") !=  null){
			Constant.QA_APK_NAME = propBuild.getProperty("build.qa.apk.name")
		}
		if(propBuild.getProperty("build.app.folderName") !=  null){
			Constant.FOLDERNAME = propBuild.getProperty("build.app.folderName")
			script.println("Folder Name: ${Constant.FOLDERNAME}")
		}

		if(propBuild.getProperty("build.app.language") !=  null){
			Constant.LANGUAGE = propBuild.getProperty("build.app.language")
			script.println("Language: ${Constant.LANGUAGE}")
		}

		if(propBuild.getProperty("build.app.firebase") !=  null){
			if (propBuild.getProperty("build.app.firebase") == "true") {
				Constant.FIREBASE_DISTRIBUTION = true
				script.println("Firebase Distribution: ${Constant.FIREBASE_DISTRIBUTION}")

			if(propBuild.getProperty("build.app.firebase.url") !=  null){
				Constant.FIREBASE_DISTRIBUTION_URL = propBuild.getProperty("build.app.firebase.url")
				script.println("Firebase Distribution URL: ${Constant.FIREBASE_DISTRIBUTION_URL}")
			}
			}
		}
		
		if(propBuild.getProperty("build.app.xcodeProjectName") !=  null){
			Constant.XCODE_PROJECT_NAME = propBuild.getProperty("build.app.xcodeProjectName")
			script.println("XCODE_PROJECT_NAME: ${Constant.XCODE_PROJECT_NAME}")
			Constant.DEV_IPA= propBuild.getProperty("build.dev.ipa.scheme")
			Constant.QA_IPA= propBuild.getProperty("build.qa.ipa.scheme")
			Constant.IPA= propBuild.getProperty("build.ipa.scheme")
			Constant.UAT_IPA= propBuild.getProperty("build.uat.ipa.scheme")
			Constant.DEV_IPA_PROFILE= propBuild.getProperty("build.dev.ipa.profile")
			Constant.QA_IPA_PROFILE= propBuild.getProperty("build.qa.ipa.profile")
			Constant.IPA_PROFILE= propBuild.getProperty("build.ipa.profile")
			Constant.UAT_IPA_PROFILE= propBuild.getProperty("build.uat.ipa.profile")
		}

        if(propBuild.getProperty("build.email.dev.web") !=  null){
			Notification.setWebDeveloperEmail(propBuild.getProperty("build.email.dev.web").toString())
			script.println("build.email.dev.android:"+Notification.getWebDeveloperEmail())
		}

		if(propBuild.getProperty("build.email.dev.android") !=  null){
			Notification.setAndroidDeveloperEmail(propBuild.getProperty("build.email.dev.android").toString())
			script.println("build.email.dev.android:"+Notification.getAndroidDeveloperEmail())
		}

		if(propBuild.getProperty("build.email.dev.ios") !=  null){
			Notification.setiOSDeveloperEmail(propBuild.getProperty("build.email.dev.ios").toString())
			script.println("build.email.dev.ios:"+Notification.getiOSDeveloperEmail())
		}

		if(propBuild.getProperty("build.email.ops") !=  null){
			String buildManagerEmails = Notification.getBuildManagerEmail().toString();
			Notification.setBuildManagerEmail(buildManagerEmails+","+propBuild.getProperty("build.email.ops")
					.toString())
			script.println("build.email.dev.ops:"+Notification.getBuildManagerEmail())
		}

		if(propBuild.getProperty("build.app.provisioningProfile") !=  null){
			Constant.PROVISIONING_PROFILE = propBuild.getProperty("build.app.provisioningProfile")
			script.println("PROVISIONING_PROFILE: ${Constant.PROVISIONING_PROFILE}")
		}

		if(propBuild.getProperty("build.docker.repo") !=  null){
			Constant.DOCKER_REPO = propBuild.getProperty("build.docker.repo")
			Constant.DOCKER_CONTAINER = propBuild.getProperty("build.docker.container")
			Constant.DOCKER_IMAGE = propBuild.getProperty("build.docker.image")
			Constant.DOCKER_NAMESPACE = propBuild.getProperty("build.docker.namespace")
			Constant.DOCKER_BUILDCONTAINER = propBuild.getProperty("build.docker.buildcontainer")
			Constant.DOCKER_SERVICE = propBuild.getProperty("build.docker.service")
			script.println("DOCKER_IMAGE: ${Constant.DOCKER_IMAGE}")
			script.println("DOCKER_REPO: ${Constant.DOCKER_REPO}")
			script.println("DOCKER_CONTAINER: ${Constant.DOCKER_CONTAINER}")
			script.println("DOCKER_BUILDCONTAINER: ${Constant.DOCKER_BUILDCONTAINER}")
			script.println("DOCKER_SERVICE: ${Constant.DOCKER_SERVICE}")
		}

		Constant.RUN_AUTO_TEST="False"
		if(propBuild.getProperty("build.automation.test") !=  null){
			Constant.RUN_AUTO_TEST=propBuild.getProperty("build.automation.test")
		}
	}

	void readAutoTestBuildProperties() {
		def buildProperties = script.readFile encoding: 'UTF-8', file: "${BUILD_PROP_FILEPATH}"

		Properties propBuild = new Properties();
		// load a properties string
		propBuild.load(new StringReader(buildProperties));

		Constant.SELENIUM_REPO_URL = propBuild.getProperty("build.repo.url")
		Constant.SELENIUM_REPO_URL = propBuild.getProperty("build.repo.branch")
	}
}

