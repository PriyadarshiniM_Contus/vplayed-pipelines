package com.contus.cd.helpers
/* Configure Tool - Maven

 Installs a Maven installation named "maven-3" if one is not already found.
 References:
 * https://github.com/batmat/jez/blob/master/jenkins-master/init_scripts/add_maven_auto_installer.groovy
 * https://wiki.jenkins-ci.org/display/JENKINS/Add+a+Maven+Installation%2C+Tool+Installation%2C+Modify+System+Config
 * https://github.com/glenjamin/jenkins-groovy-examples/blob/master/README.md


 These are the basic imports that Jenkin's interactive script console
 automatically includes.*/

class MavenSetup implements Serializable{
    String mavenName = "maven3"
    String mavenVersion = "3.5.0"

    void install(script) {
        // Grab the Maven "task" (which is the plugin handle).
        def mavenPlugin = jenkins.model.Jenkins.instance.getExtensionList(hudson.tasks.Maven.DescriptorImpl.class)[0]

        // Check for a matching installation.
        def maven3Install = mavenPlugin.installations.find {
            install -> install.name.equals(mavenName)
        }

        // If no match was found, add an installation.
        if (maven3Install == null) {
            script.println("No Maven found")
            def newMavenInstall = new hudson.tasks.Maven.MavenInstallation("${mavenName}", null,
                    [new hudson.tools.InstallSourceProperty([new hudson.tasks.Maven.MavenInstaller(mavenVersion)])]
            )

            mavenPlugin.installations += newMavenInstall
            mavenPlugin.save()
            script.println("Maven installed")

        } else {
            script.println("Maven install found. Done.")
        }
    }
}
