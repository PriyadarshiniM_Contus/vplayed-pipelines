package com.contus.cd.helpers
/**
 * Environment based env or config file creation
 *
 * @author sathishkumar@contus.in
 * @version 1.0
 */
class Environment implements Serializable {

    def script
    def jenkinsHelper
    def PROJECT_REPO_BRANCH
    Utils utils

    Environment(script, jenkinsHelper) {
        this.script = script
        this.jenkinsHelper = jenkinsHelper
        PROJECT_REPO_BRANCH = "${this.script.env.BRANCH_NAME}"
        utils = new Utils(script)
    }

	
	def createEnvForWebChat(String env) {
		script.sh "echo 'REACT_APP_SASS_API = ${saasAPIHost}\n" +
				"\n" +
				"REACT_APP_BUILD_NO=${script.env.BUILD_NUMBER}\n" +
				"REACT_APP_DATE=${utils.getDate().toString()}\n" +
				"\n" +
				"REACT_APP_GOOGLE_API_KEY = AIzaSyB5CAx4vbaejdFxi_cQAHFS37hiRwo5Lxk' > .env"
	}

    def createEnvForNode(String env) {
        String saasAPIHost = "http://versa.${env}.contus.us:3000/api/v1"
        if (env.toLowerCase().contains("qa")) {
            saasAPIHost = "http://versa.${env}.contus.us:3000/api/v1"
        } else if (env.toLowerCase().contains("uat")) {
            saasAPIHost = "http://192.168.30.50:3000"
        }
        if (PROJECT_REPO_BRANCH.toLowerCase().contains("verizon")) {
            saasAPIHost = "http://192.168.30.10:3000"
        }
        script.sh "echo 'REACT_APP_SASS_API = ${saasAPIHost}\n" +
                "\n" +
                "REACT_APP_BUILD_NO=${script.env.BUILD_NUMBER}\n" +
                "REACT_APP_DATE=${utils.getDate().toString()}\n" +
                "\n" +
                "REACT_APP_GOOGLE_API_KEY = AIzaSyB5CAx4vbaejdFxi_cQAHFS37hiRwo5Lxk' > .env"
    }

}
