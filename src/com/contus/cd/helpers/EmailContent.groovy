package com.contus.cd.helpers

import com.contus.cd.Constant

/**
 * Docker image build and deployment done here
 *
 * @author sathishkumar@contus.in
 * @version 1.0
 */
class EmailContent implements Serializable {

	def script
	def BUILD_PROP_FILEPATH = "build.properties"

	EmailContent() {}

	EmailContent(Object script) {
		this.script = script
	}

	/**
	 * Convert HTML file content to string
	 */
	String convertHTMLToString(def filename) {
		def emailContent = script.readFile encoding: 'UTF-8', file: "${filename}"
		return emailContent;
	}


	static String getSonarFailedEmailContent(def id) {
		def emailContent = this.getClass().getResource('resources/templates/sonar_failed.html')
		//def emailContent = libraryResource "templates/email_template.html"
		def str=convertHTMLToString(emailContent).replace("%MAIN_CONTENT%", 'The $PROJECT_NAME' +
				' of Build # $BUILD_NUMBER has been $BUILD_STATUS in SonarQube quality gate check, kindly fix it ASAP.')
				.replace("%SUB_CONTENT%", "URL: ${Constant.SONAR_HOST_URL}/dashboard?id=${id}")
				.replace("%TEAM%", "Build Team.");
	}
}
