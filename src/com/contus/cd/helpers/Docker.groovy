package com.contus.cd.helpers

import com.contus.cd.Constant

/**
 * Docker image build and deployment done here
 *
 * @author sathishkumar@contus.in
 * @version 1.0
 */
class Docker implements Serializable {

	def script
	def jenkinsHelper

	Docker(script, jenkinsHelper) {
		this.script = script
		this.jenkinsHelper = jenkinsHelper
	}

	/**
	 * 1. Retrieve the build properties
	 * 2. Check the build number
	 * 3. Build the docker image for the application
	 * 4. Remove the previous build/docker image and deployed build/docker container
	 * 5. Deploy the container in a specific port
	 */
	void deploy(String environment) {

		String port
		String mountVolume = ""
		Environment envConfig = new Environment(script, jenkinsHelper)
		Utils utils = new Utils(script)

		if (Constant.PLATFORM.equals("Docker-Web")) {
			port = "3001"
			envConfig.createEnvForDjango(environment)
		} else if (Constant.PLATFORM.equals("Docker-Java")) {
			port = "4000"

			// Copy Vendor Folder
			script.sh("cp -r /home/jenkins/magento-vendor/vendor/* code/vendor/")

			// Create env.php
			script.sh "mv env.txt env.php"

			// Create setup.sh file for Magento2
			Magento magento = new Magento(script, jenkinsHelper)
			magento.createSetupFile(environment)

			// Create and Mount Volume
			String volumeName = script.sh returnStdout: true, script: "docker volume ls --filter=name=magento-media " +
			"--format \"{{.Name}}\""

			if (volumeName.length() == 0) {
				script.sh "docker volume create magento-media"
			}

			mountVolume = "--mount type=volume,source=magento-media,target=/var/www/html/pub/media "

			// Bind Build Number and Date to Footer phtml
			utils.writeBuildNumberDateMagentoFooter()

		} else if (Constant.PLATFORM.toUpperCase().equals("DESIGN")) {
			port = "2000"
			utils.writeBuildNumberDateAdminDesignFooter()
		} else if (Constant.PLATFORM.toUpperCase().equals("NODE")) {
			port = "3001"
			script.dir('code') {
				envConfig.createEnvForNode(environment)
				script.sh "npm install"
			}
		}

		def LAST_SUCCESS_BUILD_NO = jenkinsHelper.getLastSuccessfulBuild()
		def LAST_SUCCESS_BUILD = "${Constant.APP_NAME}_" + "${Constant.MODULE_NAME}" + LAST_SUCCESS_BUILD_NO

		print LAST_SUCCESS_BUILD;

		def CURRENT_BUILD_NO = script.env.BUILD_NUMBER
		def NAME = "${Constant.APP_NAME}_" + "${Constant.MODULE_NAME}" + "${CURRENT_BUILD_NO}"

		try {

			// Bind Build Number and Date
			utils.writeBuildNumberDateToReleaseNote()

			// Build Docker Image --no-cache=true
			script.sh("docker build -t ${Constant.MODULE_NAME}:${NAME} --no-cache=true .")
		}
		catch (java.lang.Exception e) {
			script.currentBuild.result = "FAILURE"
			script.error "Build failed: ${e.toString()}"
		}
		/**
		 * Docker Service Implementation and Update
		 * Roll out New Version
		 */
		String SERVICE_NAME = "${Constant.APP_NAME}_" + "${Constant.MODULE_NAME}"
		String serviceName = script.sh returnStdout: true, script: "docker service ls --filter name=${SERVICE_NAME} --format " +
		"\"{{.Name}}\""
		String serviceImageName = script.sh returnStdout: true, script: "docker service ls --filter " +
		"name=${SERVICE_NAME} --format " + "\"{{.Image}}\""

		// If Service Name exist the Update Service
		if (serviceName.trim().equals(SERVICE_NAME)) {
			script.sleep 5;
			def replicas = "1"
			if (environment.toLowerCase().contains("uat")) {
				replicas = "1"
			}
			if (SERVICE_NAME.toLowerCase().contains("ecommerce")) {
				replicas = "1"
			}
			script.sh("docker service update --image ${Constant.MODULE_NAME}:${NAME} ${SERVICE_NAME} --detach=false " +
					"--replicas ${replicas}")
		} else {
			// Get the old container name. This is for the first time
			String oldContainerName = script.sh returnStdout: true, script: "docker ps --filter=name=${SERVICE_NAME} " +
			"--format \"{{.Names}}\""

			//If old container name available Remove it
			if (oldContainerName.length() != 0) {
				script.sh("docker rm -f ${oldContainerName}");
			}

			// If Service Name does not exist Create Service
			script.sh("docker service create --name ${SERVICE_NAME} --replicas 1 ${mountVolume}--publish ${port}:80 " +
					"${Constant.MODULE_NAME}:${NAME}")
		}

		// Remove the last version docker image
		if (serviceImageName.length() != 0 && (LAST_SUCCESS_BUILD_NO.toInteger() != CURRENT_BUILD_NO.toInteger())) {
			// Sleep 10 Seconds
			script.sleep 10;

			script.sh("docker rmi -f ${serviceImageName}");
		}
	}

}
