package com.contus.cd.helpers

import com.contus.cd.Constant

/**
 * Notification email and settings are being handled.
 *
 * @author sathishkumar@contus.in
 * @version 1.0
 */
class Notification implements Serializable {

    def script
    def jenkinsHelper

    static String buildManagerEmail = "priyadarshini.m@contus.in"
    static String androidDeveloperEmail = ""
    static String webDeveloperEmail = ""
    static String iOSDeveloperEmail = ""
    static String magentoDeveloperEmail = ""
    static String saasDeveloperEmail = ""
    static String designerEmail = ""
    static String qaEmail = "mirrorfly-qagroup@contus.in "

    Notification(script, jenkinsHelper) {
        this.script = script
        this.jenkinsHelper = jenkinsHelper
    }

    static String getDeveloperEmail() {
        String email
        if(Constant.PLATFORM.toUpperCase().equals("ANDROID")){
            email = Notification.getAndroidDeveloperEmail()
        }else if(Constant.PLATFORM.toUpperCase().equals("IOS")){
            email = Notification.getiOSDeveloperEmail()
        }
        else if(Constant.PLATFORM.toUpperCase().equals("WEB")){
            email = Notification.getWebDeveloperEmail()
        }else{
            email = Notification.getAndroidDeveloperEmail() +","+ Notification.getiOSDeveloperEmail()
        }

        return email
    }

}
