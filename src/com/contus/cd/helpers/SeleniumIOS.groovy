package com.contus.cd.helpers

import com.contus.cd.Constant
import com.contus.cd.helpers.JenkinsHelper


class SeleniumIOS implements Serializable {

	def script
	def jenkinsHelper
	BuildProperties buildProperties

	SeleniumIOS(Object script, JenkinsHelper jenkinsHelper) {
		this.script = script
		this.jenkinsHelper = jenkinsHelper
	}

	/**
	 * 1. Create af folder to save the project code if not exist
	 * 2. Change directory (cd) to the folder
	 * 3. Retrieve the selenium project code
	 * 4. Run the test using maven pom.xml
	 */
	void runTest(){

        // create a new folder and move ios automatio code
        script.dir("Automation") {

            // Get some code from a BitBucket repository
            script.git branch: "${Constant.IOS_SELENIUM_REPO_BRANCH}", credentialsId: "${Constant.CREDENTIAL}",
            url: "${Constant.IOS_SELENIUM_REPO_URL}"

            // Change folder path and run the test command
            script.dir("ChatApplication") {
                try{
                    // Set environment path during runtime
                    script.withEnv(["JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk-11.0.4.jdk/Contents/Home/", "PATH=/usr/local/Cellar/maven/3.6.1/bin:/usr/local/bin:/usr/local/sbin:/Users/jenkins/bin:/Library/Java/JavaVirtualMachines/jdk-11.0.4.jdk/Contents/Home/bin:/Users/jenkins/library/Android/sdk/tools:/Users/jenkins/library/Android/sdk/platform-tools:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/bin:/usr/local/bin:/usr/local/sbin:/Users/jenkins/bin:/Library/Java/JavaVirtualMachines/jdk-11.0.4.jdk/Contents/Home/bin:/Users/jenkins/library/Android/sdk/tools:/Users/jenkins/library/Android/sdk/platform-tools:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"]) {
                        def testResult = script.sh returnStdout: true, script: "mvn  test"
                        script.println("testResult: ${testResult}")
                    }
                    
                    // Sleep 5 Seconds to make sure the artifact are archived
                    script.sleep 5;
                }finally {
                    def filename="AutomationReport-${script.env.BUILD_NUMBER}.html"
                    script.println("fileName: ${filename}"	)
                    script.sh("mv **/Mirrorfly_iOS_Application_AutmReport.html ${filename}")
                    script.emailext attachmentsPattern: "${filename}", body: '$DEFAULT_CONTENT', recipientProviders:
                    [
                        [$class: 'CulpritsRecipientProvider'],
                        [$class: 'DevelopersRecipientProvider'],
                        [$class: 'RequesterRecipientProvider']
                    ], subject: '🔵 $PROJECT_NAME - Build # $BUILD_NUMBER - ' +
                    '$BUILD_STATUS', to: "${Notification.getDeveloperEmail()}, ${Notification.getBuildManagerEmail()}"
                }
            }
        }
	}
}
