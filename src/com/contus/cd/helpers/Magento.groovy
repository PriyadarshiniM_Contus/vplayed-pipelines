package com.contus.cd.helpers

class Magento implements Serializable {

    def script
    def jenkinsHelper
    def PROJECT_REPO_BRANCH

    Magento(script, jenkinsHelper) {
        this.script = script
        this.jenkinsHelper = jenkinsHelper
        PROJECT_REPO_BRANCH = "${this.script.env.BRANCH_NAME}"
    }

    void createSetupFile(String environment) {

        String dbHost = "192.168.2.28"
        String userName = "root"
        String password = "M@rooT@987"

        // Versa
        if (environment.toLowerCase().equals("uat")) {
            dbHost = "192.168.30.60"
            userName = "versadb"
            password = "Ver@Ka98Ij"
        }

        // Verizon
        if(PROJECT_REPO_BRANCH.toLowerCase().contains("verizon")){
            dbHost = "192.168.30.20"
            userName = "versadb"
            password = "Ver@Ka98Ij"
        }

        //Create setup.sh
        script.sh "echo 'cd /var/www/html/ && \\\n" +
                "   composer -g config http-basic.repo.magento.com 06d74b9b1150f125101afc9e99d1b842 5f4cc8776ac098e2e1395d03cd5b0707 && \\\n" +
                "   composer install && \\\n" +
                "   php bin/magento setup:config:set \\\n" +
                "        --db-name=versa_${environment} \\\n" +
                "        --db-host=${dbHost} \\\n" +
                "        --db-user=${userName} \\\n" +
                "        --db-password=${password}' > setup.sh"
    }

}
