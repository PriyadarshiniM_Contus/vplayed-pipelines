package com.contus.cd.helpers

import com.contus.cd.Constant
import com.contus.cd.helpers.JenkinsHelper


class Selenium implements Serializable {

	def script
	def jenkinsHelper
	BuildProperties buildProperties

	Selenium(Object script, JenkinsHelper jenkinsHelper) {
		this.script = script
		this.jenkinsHelper = jenkinsHelper
	}

	/**
	 * 1. Create af folder to save the project code if not exist
	 * 2. Change directory (cd) to the folder
	 * 3. Retrieve the selenium project code
	 * 4. Run the test using maven pom.xml
	 */
	void runTest(){

		// Get some code from a BitBucket repository
		script.git branch: "${Constant.SELENIUM_REPO_BRANCH}", credentialsId: "${Constant.CREDENTIAL}",
		url: "${Constant.SELENIUM_REPO_URL}"
		//cd ContusFly_Android
		script.dir("ChatApplication") {
			//mvn -Dmaven.test.failure.ignore=false -Dsurefire.suiteXmlFiles=testng.xml clean install
			try{
				def testResult = script.sh returnStdout: true, script: "mvn -Dmaven.test.failure.ignore=false -Dsurefire.suiteXmlFiles=testng.xml clean install "
				script.println("testResult: ${testResult}")
				// Sleep 5 Seconds to make sure the artifact are archived
				script.sleep 5;
			}finally {
				def filename="AutomationReport-${script.env.BUILD_NUMBER}.html"
				script.println("fileName: ${filename}"	)
				script.sh("mv **/Mirrorfly_Android_Application_AutmReport.html ${filename}")
				script.emailext attachmentsPattern: "${filename}", body: '$DEFAULT_CONTENT', recipientProviders:
				[
					[$class: 'CulpritsRecipientProvider'],
					[$class: 'DevelopersRecipientProvider'],
					[$class: 'RequesterRecipientProvider']
				], subject: '🔵 $PROJECT_NAME - Build # $BUILD_NUMBER - ' +
				'$BUILD_STATUS', to: "${Notification.getQaEmail()}, ${Notification.getDeveloperEmail()}, ${Notification.getBuildManagerEmail()}"

				script.archiveArtifacts "${filename}"
				//emailext attachmentsPattern: "**/Contusfly_Android_Application_AutmReport.html", attachLog: false, body: '$PROJECT_NAME - Build # $BUILD_NUMBER - ' + '$BUILD_STATUS', subject: '$PROJECT_NAME - Build # $BUILD_NUMBER - ' + '$BUILD_STATUS', to: "rameshraja.b@contus.in,harikrishnan.@contus.in"
			}
		}
		

	}
}
